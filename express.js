/*jshint node:true*/
'use strict';

var httpProxy		= require('http-proxy');
var routingProxy	= httpProxy.createProxyServer();
var apiProxy;
var express			= require('express');
var app				= express();
var bodyParser		= require('body-parser');
var compress		= require('compression');
var logger			= require('morgan');
var port			= process.env.PORT || 8001;
var environment		= process.env.NODE_ENV;

apiProxy = function(pattern, dest) {
	return function(req, res, next) {
		if (req.url.match(pattern)) {
			return routingProxy.proxyRequest(req, res, {
				changeOrigin: true,
				target: dest
			});
		} else {
			return next();
		}
	};
};

app.use(logger('dev'));

app.use(apiProxy(/\/api\/.*/, 'https://test.enroll.wisc.edu:8443/'));
app.use(apiProxy(/\/profile/, 'https://test.enroll.wisc.edu:8443/'));
app.use(apiProxy(/\/book/, 'https://test.enroll.wisc.edu:8443/'));
app.use(apiProxy(/\/scheduling/, 'https://test.enroll.wisc.edu:8443/'));
app.use(apiProxy(/\/logout/, 'https://test.enroll.wisc.edu:8443/'));

// app.use(apiProxy(/\/api\/.*/, 'https://j5dg9sv034.execute-api.us-east-1.amazonaws.com/dev'));
// app.use(apiProxy(/\/profile/, 'https://j5dg9sv034.execute-api.us-east-1.amazonaws.com/dev'));
// app.use(apiProxy(/\/book/, 'https://j5dg9sv034.execute-api.us-east-1.amazonaws.com/dev'));
// app.use(apiProxy(/\/scheduling/, 'https://j5dg9sv034.execute-api.us-east-1.amazonaws.com/dev'));
// app.use(apiProxy(/\/logout/, 'https://j5dg9sv034.execute-api.us-east-1.amazonaws.com/dev'));

// app.use(apiProxy(/\/api\/.*/, 'https://a85uiwsh1c.execute-api.us-east-1.amazonaws.com/test'));
// app.use(apiProxy(/\/profile/, 'https://a85uiwsh1c.execute-api.us-east-1.amazonaws.com/test'));
// app.use(apiProxy(/\/book/, 'https://a85uiwsh1c.execute-api.us-east-1.amazonaws.com/test'));
// app.use(apiProxy(/\/scheduling/, 'https://a85uiwsh1c.execute-api.us-east-1.amazonaws.com/test'));
// app.use(apiProxy(/\/logout/, 'https://a85uiwsh1c.execute-api.us-east-1.amazonaws.com/test'));

app.get('/my-courses', function(req, res) {
	res.sendFile(__dirname + '/src/index.html');
});
app.get('/search', function(req, res) {
	res.sendFile(__dirname + '/src/index.html');
});
app.get('/scheduler', function(req, res) {
	res.sendFile(__dirname + '/src/index.html');
});
app.get('/logout', function(req, res) {
	res.sendFile(__dirname + '/src/index.html');
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(compress());	// Compress response data with gzip

console.log('About to crank up node');
console.log('PORT=' + port);
console.log('NODE_ENV=' + environment);

app.get('/ping', function(req, res) {
	console.log(req.body);
	res.send('pong');
});

console.log('** DEV **');
app.use('/', express.static(__dirname + '/dist'));
app.use('/', express.static('./'));

app.listen(port, function() {
	console.log('Express server listening on port ' + port);
	console.log('env = ' + app.get('env') +
		'\n__dirname = ' + __dirname	+
		'\nprocess.cwd = ' + process.cwd());
});
