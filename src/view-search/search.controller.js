(function() {
	'use strict';

	angular
		.module('app.search')
		.controller('SearchController', SearchController)
		.factory('focus', function($timeout, $window) {
			return function(className) {
				$timeout(function() {
					var element = $window.document.querySelectorAll(className);
					if (element) {
						element[0].focus();
					}
				});
			};
		});

	/* @ngInject */
	function SearchController(constants, searchService, filterService, formatService, cartService, toastService, stateService, sharedService, preferencesService, focus,
			$scope, $timeout, $mdToast, $location, $route, $q, $filter, $mdDialog, $routeParams, $window, $mdMedia, $mdSidenav) {
		var vm = this;
		var isToastOpen = false;

		///////////////
		// Mock data //
		///////////////
		// TODO: Replace times with real data
		// Group needs to decide on time ranges to offer first
		// Times must only be searchable if a term is selected (because meetings times are section-level data)
		vm.times			= ['All', '8:00 AM - 10:00 AM', '10:00 AM - 12:00 PM'];
		vm.DEBUG			= constants.DEBUG;
		vm.public			= window.config && window.config.public || constants.public;
		vm.userPrefs		= preferencesService.preferences;
		vm.initialization	= true;

		//////////////////////
		// BINDABLE MEMBERS //
		//////////////////////
		vm.optShowMore					= true;
		vm.optShowSearchInvitation		= true;
		vm.terms						= [];
		vm.activeTermIndex				= 1;
		vm.subjects						= [];
		vm.seats						= [];
		vm.credits						= [];
		vm.sessions						= [];
		vm.degreePlans					= [];
		vm.specialGroups				= [];
		vm.searchResults				= [];
		vm.resultsFound					= 0;
		vm.selectedSpecialGroupCategory	= '';
		vm.courseDetails				= {};
		vm.sectionMaxLength				= [];
		vm.courseSections				= [];
		vm.searchPaneOpen				= true;
		vm.showSearchPane				= true;
		vm.showCoursePane				= false;
		vm.showSectionPane				= false;
		vm.paneFormsClass				= 'active';
		vm.paneSearchClass				= 'right';
		vm.paneCourseClass				= 'right';
		vm.paneSectionClass				= 'right';
		vm.coursePaneBack				= false;
		vm.sectionPaneBack				= false;
		vm.isSearching					= false;
		vm.hasSearchResults				= false;
		vm.hasPackages					= false;
		vm.parentPackageType			= '';
		vm.hasOptionalSection			= false;
		vm.variableCreditsException		= false;
		vm.selectedIndex				= null;
		vm.isSearchingCourse			= false;
		vm.selectedSubject				= 'All';
		vm.selectedSectionFilters		= '';
		vm.totalCoursePackages			= 0;
		vm.onSearch						= true;
		vm.filteredTotalCoursePackages	= 0;
		vm.loadingSections				= false;
		vm.sortOrder					= 'SCORE';
		vm.serviceLearningLabel			= 'Community Based Learning';

		/////////////////////
		// EXPOSED METHODS //
		/////////////////////
		vm.updateSearch					= updateSearch;
		vm.search						= search;
		vm.searchClickEvent				= searchClickEvent;
		vm.resetSearch					= resetSearch;
		vm.resultsExist					= resultsExist;
		vm.getTermDescription			= getTermDescription;
		vm.toggleAccordion				= toggleAccordion;
		vm.afterSearchPaneClosed		= afterSearchPaneClosed;
		vm.afterSearchPaneOpen			= afterSearchPaneOpen;
		vm.afterCoursePaneClosed		= afterCoursePaneClosed;
		vm.afterCoursePaneOpen			= afterCoursePaneOpen;
		vm.afterSectionPaneClosed		= afterSectionPaneClosed;
		vm.updatePanePositionClasses	= updatePanePositionClasses;
		vm.getCourseData				= getCourseData;
		vm.addToCart					= addToCart;
		vm.addToSavedLater				= addToSavedLater;
		vm.querySubjects				= querySubjects;
		vm.updateSectionFilters			= updateSectionFilters;
		vm.updateAppliedFilters			= updateAppliedFilters;
		vm.addByClassNumber				= addByClassNumber;
		vm.setReservedSections			= setReservedSections;
		vm.clearSubject					= clearSubject;
		vm.toggleSidenavSection			= toggleSidenavSection;
		vm.addToDegreePlan				= addToDegreePlan;

		// This object is used to initialize the data on the search form and to capture any changes to that data.
		// It is consumed by the search function when a search needs to be executed.
		// It is bound to various inputs via ng-model binding
		vm.formData = {
			query: $routeParams.q ? $routeParams.q : '',
			selectedTerm: $routeParams.term ? $routeParams.term : '',
			subjectCode: $routeParams.subject ? $routeParams.subject : '0000',
			session: '',
			specialGroup: '',
			foreignLanguage: '',
			workplaceExperience: false,
			ethnicStudies: $routeParams.ethnicStudies ? true : false,
			onlineOnly: false,
			gradCourseWork: false,
			serviceLearning: false,
			seatsOpen: true,
			seatsWaitlisted: true,
			seatsClosed: false,
			honorsOnly: false,
			honorsApproved: false,
			honorsLevel: false,
			levels: [
				{'key': 'E', 'value': false, 'displayName': 'Elementary'},
				{'key': 'I', 'value': false, 'displayName': 'Intermediate'},
				{'key': 'A', 'value': false, 'displayName': 'Advanced'}
			],
			breadths: [
				{'key': 'B', 'value': false, 'displayName': 'Biological Sciences'},
				{'key': 'H', 'value': false, 'displayName': 'Humanities'},
				{'key': 'L', 'value': false, 'displayName': 'Literature'},
				{'key': 'N', 'value': false, 'displayName': 'Natural Sciences'},
				{'key': 'P', 'value': false, 'displayName': 'Physical Sciences'},
				{'key': 'S', 'value': false, 'displayName': 'Social Sciences'}
			],
			foreign: [
				{'name': 'All', 'code': '' },
				{'name': '1st Semester', 'code': 'FL1' },
				{'name': '2nd Semester', 'code': 'FL2' },
				{'name': '3rd Semester', 'code': 'FL3' },
				{'name': '4th Semester', 'code': 'FL4' },
				{'name': '5th Semester', 'code': 'FL5' }
			],
			generalEd: [
				{'key': 'COM A', 'value': false, 'displayName': 'Communication A'},
				{'key': 'COM B', 'value': false, 'displayName': 'Communication B'},
				{'key': 'QR-A', 'value': false, 'displayName': 'Quantitative Reasoning A'},
				{'key': 'QR-B', 'value': false, 'displayName': 'Quantitative Reasoning B'}
			],
			days: [
				{'name': 'monday', 'value': false},
				{'name': 'tuesday', 'value': false},
				{'name': 'wednesday', 'value': false},
				{'name': 'thursday', 'value': false},
				{'name': 'friday', 'value': false}
			],
			creditsMin: '',
			creditsMax: '',
			startTime: 'All'
		};

		////////////////////////////////
		// Initialize the search view //
		////////////////////////////////
		activate();

		function activate() {
			if (!vm.public && Object.keys(vm.userPrefs).length === 0) {
				// Get user preferences
				preferencesService.getPreferences().then(function() {
					vm.userPrefs = preferencesService.preferences;
				}).catch(function(error) { console.error(error); });
			}

			vm.credits		= ['1','2','3','4','5','6','7','8','9','10','11','12'];
			vm.appliedFilters = [];
			vm.modeOfInstruction = [
				{'name': 'All', 'value': ''},
				{'name': 'Classroom Only', 'value': 'Classroom Instruction'},
				{'name': 'Online (Some Classroom)', 'value': 'Online (some classroom)'},
				{'name': 'Online Only', 'value': 'Online Only'}
			];

			if (stateService.searchResults) {
				vm.hasSearchResults = true;
				vm.optShowSearchInvitation = false;
				vm.formData = stateService.searchForm;
				vm.resultsFound = stateService.resultsFound;
				vm.searchResults = stateService.searchResults;
				vm.selectedSubject = stateService.selectedSubject;

				vm.showCoursePane = stateService.showCoursePane;
				vm.showSectionPane = stateService.showSectionPane;

				if (stateService.selectedCourseIndex) {
					var selectedCourse = vm.searchResults.items[stateService.selectedCourseIndex];
					vm.courseDetails = selectedCourse;
					vm.selectedIndex = stateService.selectedCourseIndex;
					vm.searchResultsSectionFilters = stateService.appliedFilters;
					vm.appliedFilters = stateService.appliedFilters;
					getCourseSections(selectedCourse.courseId, selectedCourse.subject.subjectCode, stateService.appliedFilters);
				}
			}

			getTerms().then(function() {
				if (localStorage.selectedTerm) {
					if (localStorage.selectedTerm === 'undefined') {
						localStorage.setItem('selectedTerm', vm.terms[1].termCode);
					}
					var selectedTerm = localStorage.selectedTerm.toString();
					if ($routeParams.term) {
						selectedTerm = $routeParams.term;
						localStorage.selectedTerm = $routeParams.term;
					}
					vm.terms.forEach(function(termObj, i) {
						if (termObj.termCode === selectedTerm) {
							vm.activeTermIndex = i;
						}
					});
					if (!vm.public) $scope.$emit('cart-action', [selectedTerm, 'load-search']);
				} else {
					localStorage.setItem('selectedTerm', vm.terms[1].termCode);
					vm.selectedTerm = vm.terms[1].termCode;
				}
				if ((localStorage.seatsOpen) || (localStorage.seatsWaitlisted) || (localStorage.seatsClosed)) {
					vm.formData.seatsOpen = (localStorage.seatsOpen === 'true');
					vm.formData.seatsWaitlisted = (localStorage.seatsWaitlisted === 'true');
					vm.formData.seatsClosed = (localStorage.seatsClosed === 'true');
				}
				getSessions();
				if (!vm.public) getDegreePlans();
				getSpecialGroups();
				getSubjects().then(function() {
					if ($routeParams.subject) {
						vm.selectedSubject = vm.subjects.filter(function(subject) {
							return subject.subjectCode == $routeParams.subject;
						})[0];
					}

					// Watches the subject dropdown and triggers the auto search
					$scope.$watch('vm.selectedSubject', function(newValue, oldValue) {
						if ((newValue !== null) && (newValue != oldValue)) {
							vm.formData.subjectCode = vm.selectedSubject.subjectCode;
						}
					});

					// Any form interactions trigger a search, but we don't want this to happen on page load while it's setting default values
					$scope.$watch('vm.formData', function(formDataNew, formDataOld) {
						if (vm.initialization) {
							$timeout(function() {
								vm.initialization = false;
							});
						} else {
							// Use local storage to save the term selected
							if ((localStorage.selectedTerm === undefined) || (localStorage.selectedTerm === 'undefined') || (formDataNew.selectedTerm != localStorage.selectedTerm.toString())) {
								localStorage.setItem('selectedTerm', formDataNew.selectedTerm);
								$location.search('term', formDataNew.selectedTerm);
								$route.reload();
							}
							if ((localStorage.seatsOpen === undefined) || (formDataNew.seatsOpen != localStorage.seatsOpen.toString())) {
								localStorage.setItem('seatsOpen', formDataNew.seatsOpen);
							}
							if ((localStorage.seatsWaitlisted === undefined) || (formDataNew.seatsWaitlisted != localStorage.seatsWaitlisted.toString())) {
								localStorage.setItem('seatsWaitlisted', formDataNew.seatsWaitlisted);
							}
							if ((localStorage.seatsClosed === undefined) || (formDataNew.seatsClosed != localStorage.seatsClosed.toString())) {
								localStorage.setItem('seatsClosed', formDataNew.seatsClosed);
							}
							if (formDataOld != formDataNew) {
								if (formDataOld.query != formDataNew.query) return;
								if (formDataOld.selectedTerm != formDataNew.selectedTerm) return;
								updateSearch();
							}
						}
					}, true);
				}).catch(function(error) { console.error(error); });

				if (Object.keys($routeParams).length) {
					buildFilters();
					var filters = [];
					if ($routeParams.courseId) filters.push({"term":{"courseId":$routeParams.courseId.toString()}});
					if ($routeParams.subject) filters.push({"term":{"subject.subjectCode":$routeParams.subject.toString()}});
					if ($routeParams.ethnicStudies) filters.push({"term":{"ethnicStudies.code":"ETHNIC ST"}});
					if ($routeParams.topicId) filters.push({"term":{"topics.id":$routeParams.topicId.toString()}});
					if ($routeParams.catalogNum) filters.push({"term":{"catalogNumber":$routeParams.catalogNum.toString()}});
					if ($routeParams.instructionMode) filters.push({"has_child":{"type":"enrollmentPackage","query":{"multi_match":{"query":$routeParams.instructionMode,"type":"phrase","fields":["sections.instructionMode"]}}}});
					if ($routeParams.attrCode) filters.push({"has_child":{"type":"class","filter":{"term":{"classAttributes.attributeCode":$routeParams.attrCode}}}});
					if ($routeParams.attrValue) filters.push({"has_child":{"type":"class","filter":{"term":{"classAttributes.valueCode":$routeParams.attrValue}}}});

					var query = {
						'filters': filters,
						'page': 1,
						'pageSize': 20,
						'queryString': $routeParams.q || '*',
						'selectedTerm': $routeParams.term || vm.terms[vm.activeTermIndex].termCode,
						'sortOrder': "SCORE"
					};

					vm.optShowSearchInvitation = false;
					vm.search(query);
				}
			});

			return;
		}
		////////////////////
		// EVENT HANDLERS //
		////////////////////

		/**
		 * Fire Search function. Called whenever UI must change in response to user interactions (ex. when a term is selected).
		 */
		function updateSearch() {
			$timeout(function() {
				angular.element('.btn-search').trigger('click');
			}, 100);
		}

		function querySubjects(query) {
			var results = query ? vm.subjects.filter(startsWithFilter(query)) : vm.subjects;
			return results;
		}

		function startsWithFilter(query) {
			var lowercaseQuery = angular.lowercase(query);

			return function filterFn(subjects) {
				return (subjects.formalDescription.toLowerCase().indexOf(lowercaseQuery) === 0);
			};
		}

		function clearSubject($event) {
			// $scope.$$childHead.$$nextSibling.$$nextSibling.$mdAutocompleteCtrl.focus();
			if($event.type != 'keydown') {
				vm.selectedSubject = null;
				vm.subjectSearchText = '';
				console.log('sub cleared', $event);
			}
		}

		/**
		* Search Notes
		*-------------------
		* There are multiple ways to search based on animations. Since XHR requests cause animations to studder we are making all XHR requests after an animation
		* has completed.
		* Scenario 1: Set on new page load, all panes are collapsed. A search is fired and the results pane opens and the XHR is fire when the animation completes.
		* Scenario 2: Search Pane is open with results. We do not want to wait for the animation in this case since the pane is already open so we bypass that code.
		* Scenario 3: Both Search Pane and Course Pane are open. A user enters a new search term and hits enter. The course pane must close before the XHR for search
		* is fired so we wait until the Course pane closes then fire the XHR
		* Scenario 4: Both Search Pane and Course Pane are open with search results and a user clicks the back button on the Course Pane. We do not want the search
		* to fire again in this case so we bypass that code.
		* Scenario 5: User has a search and course pane open and then changes the term,subject,seat. The search click event is fired, but on mobile this is disabled.
		*/


		/**
		 * Triggers the search after the opening pane animation is complete. This was the only way to avoid the animation stutter that happens during an XHR
		 */
		function afterSearchPaneOpen() {
			if (!vm.initialization) {
				// Make sure course pane is closed
				vm.showSectionPane = false;

				if (vm.searchPaneOpen === false) {
					vm.searchPaneOpen = true;
					vm.search();
				}
			}
		}

		/**
		 * Fires when a search happens for the first time so the animation displays correctly.
		 */
		function searchClickEvent() {
			vm.optShowSearchInvitation = false;
			vm.showSearchPane = true;
			vm.isSearching = true;
			vm.updatePanePositionClasses();
		}

		function resetSearch() {
			// clear state
			stateService.searchResults = false;
			$location.search('term', null);
			$location.search('subject', null);
			$location.search('q', null);
			$location.search('courseId', null);
			$location.search('subjectCode', null);
			$location.search('ethnicStudies', null);
			$location.search('topicId', null);
			$location.search('catalogNum', null);
			$location.search('instructionMode', null);
			$location.search('attrCode', null);
			$location.search('attrValue', null);
			vm.formData.seatsOpen = true;
			localStorage.seatsOpen = true;
			vm.formData.seatsWaitlisted = true;
			localStorage.seatsWaitlisted = true;
			vm.formData.seatsClosed = false;
			localStorage.seatsClosed = false;
			$route.reload();
		}

		/**
		 * When the search pane closes remove any existing search results for any subsequent searchs
		*/
		function afterSearchPaneClosed() {
			vm.searchPaneOpen = false;
			vm.optShowSearchInvitation = false;
			vm.isSearching = true;
			vm.courseDetails = {};
			vm.courseSections = [];
			vm.searchResults = [];
		}

		function afterCoursePaneOpen() {
			vm.coursePaneOpen = true;
			if ($mdMedia('sm') || $mdMedia('xs')) {
				getCourseSections(vm.courseDetails.courseId, vm.courseDetails.subject.subjectCode, vm.appliedFilters);
				$timeout(function() {vm.loadingSections = false; }, 500);
			}
		}

		function afterCoursePaneClosed() {
			if (!vm.initialization) {
				// If the user clicks the back button we don't want to submit another search
				if (vm.coursePaneBack === false) {
					vm.isSearching = true;
					vm.search();
				}
				vm.coursePaneBack = false;
			}
		}

		function afterSectionPaneClosed() {
			if (!vm.initialization) {
				if (vm.sectionPaneBack === false) {
					vm.showCoursePane = false;
				}
				vm.sectionPaneBack = false;
			}
		}

		/**
		 * Sets classes to the panes for the scrolling effect. Primarily works on mobile.
		*/
		function updatePanePositionClasses() {
			if ((!vm.showSearchPane) && (!vm.showCoursePane)) {
				vm.paneFormsClass = 'active';
				vm.paneSearchClass = 'right';
				vm.paneCourseClass = 'right';
				vm.paneSectionClass = 'right';
			} else if ((vm.showSearchPane) && (!vm.showCoursePane)) {
				vm.paneFormsClass = 'left';
				vm.paneSearchClass = 'active';
				vm.paneCourseClass = 'right';
				vm.paneSectionClass = 'right';
			} else if ((vm.showSearchPane) && (vm.showCoursePane) && (!vm.showSectionPane)) {
				vm.paneFormsClass = 'left';
				vm.paneSearchClass = 'left';
				vm.paneCourseClass = 'active';
				vm.paneSectionClass = 'right';
			} else if ((vm.showSearchPane) && (vm.showCoursePane) && (vm.showSectionPane)) {
				vm.paneFormsClass = 'left';
				vm.paneSearchClass = 'left';
				vm.paneCourseClass = 'left';
				vm.paneSectionClass = 'active';
			}
			stateService.showCoursePane = vm.showCoursePane;
			stateService.showSectionPane = vm.showSectionPane;

		}

		function toggleAccordion(event) {
			var parent = event.currentTarget.parentElement,
			arrow = parent.getElementsByClassName('toggle-arrow')[0],
			arrowType = arrow.innerHTML;

			// Toggle collapsed/expanded
			if (parent.classList.contains('collapsed')) {
				parent.className = parent.className.replace('collapsed', 'expanded');
				parent.setAttribute('aria-expanded', true);
			} else if (parent.classList.contains('expanded')) {
				parent.className = parent.className.replace('expanded', 'collapsed');
				parent.setAttribute('aria-expanded', false);
			}

			// Toggle arrow type
			if (arrowType == 'keyboard_arrow_down') {
				arrow.innerHTML = 'keyboard_arrow_up';
			} else if (arrowType == 'keyboard_arrow_up') {
				arrow.innerHTML = 'keyboard_arrow_down';
			}
		}

		/////////////
		// GETTERS //
		/////////////
		function getTerms() {
			return searchService.getTerms()
				.then(function success(data) {
					var terms = [];
					// Add 'any' option
					terms.push({'longDescription': 'All', termCode: '0000' });
					// Add the rest of the terms
					for (var i in data) {
						var term = {
							longDescription: getTermDescription(data[i].termCode, true),
							termCode: data[i].termCode
						};
						terms.push(term);
					}
					vm.terms = terms;
					// if (vm.DEBUG)console.info('terms:', terms);
					return vm.terms;
				});
		}
		/**
		* Takes termCode id and determine a short or long description of the code i.e. fall 2016 or fall 2016-2017
		* termCode logic is a follows:
		*    First digit is century, counting from 20 = 1, second and third are year, fourth is ‘semester'
		*    1172 = (1 (century), 17 (2016-2017 academic year), 2 (fall)
		*    Last digit: 2 (fall), 3 (winter), 4 (spring), 6 (summer)
		* All terms are displayed with the year before to account for fall 2016 in the above example
		* This is true for all terms except summer.
		*
		* @param {String} id - for the termCode
		* @param {bool} isShortDescription - Boolean flag for when a term description with a single year is desired
		* @returns {String} - string Either the short or long term description
		*/
		function getTermDescription(id, isShortDescription) {
			if (id) {
				var termDescription = '';
				var splitId = id.split('');
				var century = (19 + Number(splitId[0])).toString();
				var year = Number(splitId.splice(1, 2).join(''));
				var term = Number(splitId[1]);

				switch (term) {
					case 2:
						termDescription = 'Fall ' + century;
						if (isShortDescription) termDescription += (year - 1).toString();
						break;
					case 3:
						//Replacing winter with more standard display of Fall
						termDescription = 'Fall ' + century;
						if (isShortDescription) termDescription += (year - 1).toString();
						break;
					case 4:
						termDescription = 'Spring ' + century;
						if (isShortDescription) termDescription += (year).toString();
						break;
					case 6:
						termDescription = 'Summer ' + century;
						if (isShortDescription) termDescription += (year).toString();
						break;
				}
				if (!isShortDescription) {
					termDescription += (term === 6) ? year.toString() : (year - 1).toString() + '-20' + year.toString();
				}
				return termDescription;
			}
		}
		/**
		 * Get the searchable subjects
		 *
		 * @returns {*} Objects containing information about subjects
		 */
		function getSubjects() {
			var term = vm.terms[vm.activeTermIndex].termCode;
			return searchService.getSubjects()
				.then(function success(data) {
					var subjects = [];
					angular.forEach(data, function(key, value) {
						if (value == term) {
							for (var i in key) {
								var subject = {
									formalDescription: formatService.formatSubject(key[i].formalDescription),
									subjectCode: key[i].subjectCode
								};
								subjects.push(subject);
							}
						}

					});
					vm.subjects = formatService.alphabetizeObjects(subjects, 'formalDescription');
					vm.subjects.unshift({'formalDescription': 'All', subjectCode: '0000'});
					return vm.subjects;
				});
		}

		/**
		 * Get the searchable sessions (no results unless a term is selected)
		 *
		 * @returns {*} Objects containing information about sessions
		 */
		function getSessions() {
			return searchService.getSessionsByTerm(vm.terms[vm.activeTermIndex].termCode)
				.then(function success(data) {
					var sessions = [];
					angular.forEach(data, function(key) {
						if (key.hasOwnProperty('beginDate')) {
							key.beginDate = moment.unix(key.beginDate / 1000).format('MMM D YYYY');
						}
						if (key.hasOwnProperty('endDate')) {
							key.endDate = moment.unix(key.endDate / 1000).format('MMM D, YYYY');
						}
						sessions.push(key);
					});
					vm.sessions = sessions;
					return vm.sessions;
				});
		}

		function getDegreePlans() {
			return cartService.getDegreePlans().then(function(data) {
				vm.degreePlans = data;
			});
		}

		/**
		 * Get special groups for the selected term
		 *
		 * @returns {*}
		 */
		function getSpecialGroups() {
			return searchService.getSpecialGroupsByTerm(vm.terms[vm.activeTermIndex].termCode)
				.then(function success(data) {
					// Get all special group attributes
					var specialGroupAttributes = getSpecialGroupAttributes(data);
					// Build special groups with attributes
					vm.specialGroups = buildSpecialGroups(specialGroupAttributes, data);
					vm.specialGroups.unshift({ attributeDisplayName: 'All', values: [{ valueShortName: 'All', valueCode: '' }] });
					vm.serviceLearningLabel = data.filter(function(serviceLearning) { return serviceLearning.attributeCode == 'SVCL'; })[0].valueDescription;
					return vm.specialGroups;
				});

			// Get attribute categories of special groups
			function getSpecialGroupAttributes(data) {
				var attributes = [];
				angular.forEach(data, function(key) {
					var attribute = {
						attributeCode: key.attributeCode,
						attributeDisplayName: key.attributeDisplayName
					};
					// Add each special group category only once
					if (attributes.filter(function(each) { return each.attributeCode == key.attributeCode; }).length <= 0) {
						attribute.values = [];
						attributes.push(attribute);
					}
				});
				return attributes;
			}

			// Build special groups categories and children
			function buildSpecialGroups(groups, data) {
				angular.forEach(data, function(key) {
					var value = {
						valueCode: key.valueCode,
						valueShortName: key.valueShortName
					};
					for (var i in groups) {
						var group = groups[i];
						if (group.attributeCode === key.attributeCode) {
							group.values.push(value);
						}
					}
				});
				return groups;
			}
		}

		////////////
		// SEARCH //
		////////////
		/**
		 * Combine all search parameters from the form and search for courses
		 *
		 * @returns {*} JSON object containing a list of courses
		 */
		function search(query) {

			// Bail early if the pane is closed. we want to fire the search after the animation.
			if (!vm.searchPaneOpen) {
				return;
			}

			// If the course pane is open bail, we want the search that's after the animation.
			if (vm.showCoursePane) {
				vm.showCoursePane = false;
				return;
			}

			vm.updatePanePositionClasses();

			// Initialize/reset courseDetails and searchResults objects
			vm.courseDetails = {};
			vm.courseSections = [];
			vm.searchResults = {};
			vm.hasSearchResults = false;
			vm.isSearching = true;
			vm.selectedIndex = null;



			if (!query) {
				// Build filters
				var filters = buildFilters();

				// Initialize/reset query string
				var queryString = '*';
				if (vm.formData.query !== '') {
					queryString = angular.copy(vm.formData.query);
				}
				var query = {
					'filters': filters,
					'page': 1,
					'pageSize': 20,
					'queryString': queryString,
					'selectedTerm': vm.formData.selectedTerm,
					'sortOrder': vm.sortOrder
				};
			}
			// Create query object for back end consumption


			$location.search('q', vm.formData.query !== '' ? vm.formData.query : null);
			$location.search('term', vm.formData.selectedTerm !== '' ? vm.formData.selectedTerm : null);
			$location.search('subject', vm.formData.subjectCode !== '0000' ? vm.formData.subjectCode : null);

			// Only push ga event in prod
			if (!vm.DEBUG) {
				$window.ga('send', 'pageview', {page: $location.url()});
			}

			vm.searchResultsSectionFilters = angular.copy(vm.appliedFilters);

			stateService.appliedFilters = vm.searchResultsSectionFilters;
			stateService.selectedSubject = vm.selectedSubject;
			stateService.searchForm = vm.formData;

			vm.searchResults = {
				stopSearching: false,
				hold: false,
				numLoaded_: 0,
				toLoad_: 0,
				items: [],
				getItemAtIndex: function(index) {
					if (!this.hold) {
						if (index > this.numLoaded_) {
							this.fetchMoreItems_(index);
							return null;
						}
					}
					return index;
				},
				getLength: function() {
					if (!this.stopSearching) {
						return this.numLoaded_ + 10;
					} else {
						return this.numLoaded_;
					}
				},
				fetchMoreItems_: function(index) {
					if (this.toLoad_ < index) {
						this.hold = true;
						this.toLoad_ += 20;

						searchService.searchWithFilters(query).then(angular.bind(this, function(data) {
							if (data) {
								if (vm.DEBUG)console.info('searchResults:', data);

								vm.resultsFound = data.found;
								stateService.resultsFound = data.found;

								if (data.hits && data.hits.length > 0) {
									vm.hasSearchResults = true;
									stateService.searchResults = vm.searchResults;
									this.items = this.items.concat(data.hits);

									var numHits = data.hits.length;
									if (numHits < query.pageSize) {
										this.stopSearching = true;
										this.numLoaded_ = data.found;
									} else {
										this.numLoaded_ = this.items.length;
										++query.page;
										this.hold = false;
									}
								} else {
									if (!vm.resultsFound){
										vm.hasSearchResults = false;
										vm.resultsFound = 0;
										vm.optShowSearchInvitation = false;
									}
									this.stopSearching = true;
								}
								vm.isSearching = false;
							}
						})).catch(function(error) { console.error(error); });
					}
				}
			};
		}

		/*
		 * Gets course data for a specific course. Used in the search results list item directived
		 * @param course The course to get details and sections for
		 */
		function getCourseData(course) {
			vm.selectedSection = false;
			vm.sectionMaxLength = false;
			vm.loadingSections = true;
			vm.appliedFilters = angular.copy(vm.searchResultsSectionFilters);
			getCourseDetails(course.courseId, course.subject.subjectCode, course.subject.termCode);
			if (vm.coursePaneOpen && $mdMedia('xs') || vm.coursePaneOpen && $mdMedia('sm')) {
				getCourseSections(course.courseId, course.subject.subjectCode);
				$timeout(function() {vm.loadingSections = false; }, 500);
				if ($mdMedia('gt-md')) focus('.pane__course-details');
			}
		}

		/**
		 * Get the details for a specific course to pass on to the course-details directive and get
		 * displayable data for the "last taught" property of the course

		 * @param {String} courseId - The course ID of the selected course
		 * @param {String} subjectCode - The subject code of the selected course
		 * @returns {*} A single course object
		 */
		function getCourseDetails(courseId, subjectCode, termCode) {

			vm.showCoursePane = true;
			vm.updatePanePositionClasses();

			// Get the course that was selected
			for (var i in vm.searchResults.items) {
				if (vm.searchResults.items[i].courseId == courseId && vm.searchResults.items[i].subject.subjectCode == subjectCode && vm.searchResults.items[i].subject.termCode == termCode) {
					vm.courseDetails = vm.searchResults.items[i];
					vm.selectedIndex = i;
					stateService.selectedCourseIndex = vm.selectedIndex;
					if (vm.DEBUG)console.info('courseDetails:', vm.courseDetails);
				}
			}
		}

		/**
		 * Get the course's available sections
		 *
		 * @param {String} courseId - The ID of the selected course
		 * @param {String} subjectCode - The subject code of the selected course
		 * @returns {*} Response object containing a list of sections
		 */
		function getCourseSections(courseId, subjectCode) {
			// Variables that need to get reset each time this function is called.
			vm.hasPackages = false;
			vm.isSearchingCourse = true;
			vm.hasOptionalSection = false;
			if (vm.formData.selectedTerm !== '') {
				return searchService.getPackages(vm.formData.selectedTerm, courseId, subjectCode)
					.then(function success(data) {

						/* We need to refactor the data to treat a lecture as a parent and any labs, discussions, etc. as children since this
						 * is how we are grouping the lectures in the UI. There is an edge case where there are no lectures but a package that
						 * consists of a lab and discussion. In this case, we want the lab to be the parent.
						 * The first part of this code is designed to find out what the parent properties are, then we create a packages property
						 * on the parent object.
						 */

						// If there is only 1 section (Lecture) in a package we assume there are no packages and hasPackages remains false.
						var allLectures = data.map(function(coursePackage) {
							var lecture = coursePackage.sections.filter(function(section) {
								// Set package values on the sections since we display the sections in the UI
								section.packageEnrollmentStatus = coursePackage.packageEnrollmentStatus;
								section.enrollmentRequirementGroups = coursePackage.enrollmentRequirementGroups;
								section.enrollmentOptions = coursePackage.enrollmentOptions;
								section.instructorProvidedClassDetails = coursePackage.instructorProvidedClassDetails;
								if (coursePackage.creditRange) {
									section.creditRange = coursePackage.creditRange;
								}
								if (!vm.hasPackages && coursePackage.sections.length > 1) {
									vm.hasPackages = true;
								}
								return true;
							});
							return lecture[0];
						});

						// Set the name of the parent section
						if (allLectures[0]) {
							vm.parentPackageType = allLectures[0].type;
						}

						if (vm.hasPackages) {
							// Get the section numbers from allLectures array then filter out the dups
							var sectionNums = allLectures.map(function(e) { return e.sectionNumber; });
							sectionNums = sectionNums.filter(function(item, pos) { return sectionNums.indexOf(item) == pos; });

							// Reduce allLectures array to only 1 lecture per section number
							var lectures = sectionNums.map(function(section) {
								for (var i = allLectures.length - 1; i >= 0; i--) {
									if (allLectures[i].sectionNumber == section) {
										return angular.copy(allLectures[i]);
									}
								}
							});

							var OptionalLecturePackages = [];
							// Search the 'data' for all packages and add a packages property to each lecture
							sectionNums.forEach(function(sectionNum) {
								var lectureWithPackages = [];
								for (var i = data.length - 1; i >= 0; i--) {
									for (var j = data[i].sections.length - 1; j >= 0; j--) {
										if ((data[i].sections[j].type == vm.parentPackageType) && (data[i].sections[j].sectionNumber == sectionNum)) {
											//Set the classNumber for the package used for enrollment on the parentPackageType
											data[i].sections[j].enrollmentClassNumber = data[i].enrollmentClassNumber;
											data[i].sections[j].instructorProvidedClassDetails = data[i].instructorProvidedClassDetails;
											if (data[i].sections.length > 1) {
												data[i].sections[j + 1].packageEnrollmentStatus = data[i].packageEnrollmentStatus;
												vm.sectionMaxLength = data[i].sections;
												lectureWithPackages.push(data[i].sections);
											} else {
												data[i].sections[j].packageEnrollmentStatus = data[i].packageEnrollmentStatus;
												var tmpLecture = angular.copy(data[i].sections[0]);
												tmpLecture.packages = [data[i].sections];
												OptionalLecturePackages.push(tmpLecture);
											}
										}
										if (data[i].sections.length == 1) {
											// Packages always have the same length of classes until they don't, which means the lab for instance is optional
											// in this case we need to modify its display on the front end.
											vm.hasOptionalSection = true;
										}
									}
								}
								for (var k = lectures.length - 1; k >= 0; k--) {
									if (lectures[k].sectionNumber == sectionNum) {
										lectures[k].packages = [];
										// lectures[k].packages = JSON.parse(JSON.stringify(lectureWithPackages));
										lectureWithPackages.map(function(section) {
											return lectures[k].packages.push(section);
										});
									}
								}
							});
							lectures = lectures.concat(OptionalLecturePackages);
							vm.courseSections = lectures;
						} else {
							vm.sectionMaxLength = [allLectures[0]];
							vm.courseSections = allLectures;
						}
						if (vm.DEBUG)console.info('Refactored courseSections before filters:', vm.courseSections);

						vm.courseSectionsData = angular.copy(vm.courseSections);
						vm.selectedSectionFilters = vm.appliedFilters;
						updateSectionFilters();

						vm.isSearchingCourse = false;

						if (vm.DEBUG)console.info('courseSections:', data);
						if (vm.DEBUG)console.info('Refactored courseSections:', vm.courseSections);

						vm.loadingSections = false;
					}).catch(function(error){ console.error(error); });
			}
		}

		function updateAppliedFilters() {
			vm.appliedFilters = vm.selectedSectionFilters;
			updateSectionFilters();
		}

		function updateSectionFilters() {
			var numFilters = vm.appliedFilters.length;
			vm.totalCoursePackages = 0;
			vm.filteredTotalCoursePackages = 0;
			vm.courseSections = angular.copy(vm.courseSectionsData);

			if (vm.hasPackages) {
				vm.courseSectionsData.forEach(function(coursePackages) { vm.totalCoursePackages += coursePackages.packages.length; });
			} else {
				vm.totalCoursePackages = vm.courseSectionsData.length;
			}

			if ((vm.showSectionPane === true) && (!numFilters) || vm.courseSections.length === 0) {
				vm.courseSections = angular.copy(vm.courseSectionsData);
				vm.filteredTotalCoursePackages = vm.totalCoursePackages;
				return;
			}

			var t0 = performance.now();
			vm.courseSections = $filter('sectionFilter')(vm.courseSections, vm.appliedFilters, vm.formData.specialGroup);
			var t1 = performance.now();
			if (vm.DEBUG) console.log('section filtering took ' + (t1 - t0) + ' milliseconds.');

			if (vm.hasPackages) {
				vm.courseSections.forEach(function(coursePackages) { vm.filteredTotalCoursePackages += coursePackages.packages.length; });
			} else {
				vm.filteredTotalCoursePackages = vm.courseSections.length;
			}
		}

		function addByClassNumber(event) {
			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: event,
				template: '<md-dialog aria-label="section reserved-sections" style="width:500px">' +
							'<md-toolbar class="md-primary">' +
								'<div class="md-toolbar-tools">Enter a class number.</div>' +
							'</md-toolbar>' +
							'<form name="classNumberForm">' +
								'<md-dialog-content class="md-dialog-content">' +
									'<md-input-container class="md-block md-primary">' +
										'<label>Class number</label>' +
										'<input type="text" class="" name="classNumberInput" ng-model="classNumber" aria-label="add course by class number" placeholder="Class number" ng-pattern="/^[0-9]{5}$/" md-maxlength="5"/>' +
										'<div ng-messages="classNumberForm.classNumberInput.$error" role="alert">' +
											'<div ng-message="md-maxlength" class="my-message">number must be 5 digits</div>' +
											'<div ng-message="pattern" class="my-message">invalid class number</div>' +
										'</div ng-messages>' +
									'</md-input-container>' +
									'<p><strong>Note:</strong> Please enter an enrollment class number and check credits for a variable credit course then add to cart.</p>' +

									'<md-input-container class="md-block md-primary section-options" ng-show="packageData.length > 1">' +
										'<label>Select Class:</label>' +
										'<md-select ng-model="selectedPackage" class="md-primary" aria-label="select class" ng-change="vm.myCoursesAddToCart()" required>' +
											'<md-option ng-repeat="match in packageData" ng-value="match" class="md-primary"><span class="semi-bold">{{ match._source.sections[0].subject.shortDescription }} {{ match._source.catalogNumber }}</span><span ng-repeat="section in match._source.sections">&nbsp;{{ section.type }} {{ section.sectionNumber }}{{ match._source.sections.length > 1 ? "," : ""  }}</span>{{ match._source.sections.length > 1 ? "" : ","  }} credits: {{ match._source.creditRange }}</md-option>' +
										'</md-select>' +
									'</md-input-container>' +

									'<md-input-container class="md-block md-primary section-options" ng-show="showCreditsDropdown">' +
										'<label>Select Credits</label>' +
										'<md-select ng-model="selectedCredits" class="md-primary" aria-label="select credits" ng-change="vm.myCoursesAddToCart()" required>' +
											'<md-option ng-repeat="credit in creditRange" value="{{credit}}" class="md-primary">{{credit}} {{ credit == 1 ? "Credit" : "Credits"  }}</md-option>' +
										'</md-select>' +
									'</md-input-container>' +

									'<span ng-hide="showCreditsDropdown || !creditCheckComplete || packageData.length > 1"><p>Credits set to: {{ packageData[0]._source.creditRange }}</p></span>' +
								'</md-dialog-content>' +
								'<md-dialog-actions layout="row" class="layout-row">' +
									'<md-button ng-click="checkCredits()" class="md-primary" aria-label="close dialog" ng-disabled="creditCheckComplete || classNumberForm.classNumberInput.$invalid || classNumberForm.classNumberInput.$pristine">Check credits</md-button>' +
									'<span flex="" class="flex"></span>' +
									'<md-button ng-click="closeDialog()" class="md-primary" aria-label="close dialog">Cancel</md-button>' +
									'<md-button ng-click="classNumberAddToCart()" class="md-raised md-primary md-button" aria-label="add to cart" ng-hide="(hasOnePackage || hasMultiplePackages)" ng-disabled="!creditCheckComplete">Add to cart</md-button>' +
									'<md-button ng-click="classNumberAddToCart()" class="md-raised md-primary md-button" aria-label="add to cart" ng-show="hasMultiplePackages" ng-disabled="(!selectedPackage || !selectedCredits)">Add to cart</md-button>' +
									'<md-button ng-click="classNumberAddToCart()" class="md-raised md-primary md-button" aria-label="add to cart" ng-show="hasOnePackage" ng-disabled="!selectedCredits">Add to cart</md-button>' +
								'</md-dialog-actions>' +
							'</form>' +
						'</md-dialog>',
				locals: {
					term: vm.formData.selectedTerm
				},
				clickOutsideToClose: true,
				controller: ReservedController
			}, function error() {});

			/* @ngInject */
			function ReservedController($scope, $mdDialog, searchService, term) {
				$scope.classNumber = '';
				$scope.term = term;
				$scope.selectedCredits = 0;

				$scope.closeDialog = function() {
					$mdDialog.hide();
				};

				$scope.checkCredits = function() {

					function range(start, stop) {
						var a = [start], b = start;
						while (b < stop) {b += 1; a.push(b);}
						return a;
					}

					searchService.getCourseInfo(term, $scope.classNumber)
					.then(function success(data) {
						if (data.length) {
							$scope.classData = data[0];
							var query = '{"size": 200, "query": { "match": { "enrollmentClassNumber": ' + $scope.classNumber + ' }}}';
							searchService.getEnrollmentPackageByNumber($scope.classData.classUniqueId.termCode, query).then(function(data) {
								if (!data.hits.total) {
									toastService.infoMessage('That class number was not an enrollment class number.');
								} else {
									// Filter out courses that are children of parent topics (58451.22) We want the parent topics course (58451)
									$scope.packageData = data.hits.hits.filter(function(course) {
										return !course._source.hasOwnProperty('topic');
									});
									$scope.packageData.forEach(function(course) {
										if (course._source.creditRange.indexOf('-') !== -1) {
											var min = Number(course._source.creditRange.split('-')[0]),
												max = Number(course._source.creditRange.split('-')[1]);

											$scope.showCreditsDropdown = true;
											$scope.creditRange = range(min, max);
											($scope.packageData.length === 1) ? $scope.hasOnePackage = true : $scope.hasOnePackage = false;
											($scope.packageData.length > 1) ? $scope.hasMultiplePackages = true : $scope.hasMultiplePackages = false;
										}
										$scope.creditCheckComplete = true;
									});
								}
							}).catch(function(error) { console.error(error);});
							}
							else {
								toastService.infoMessage('No courses were found with that class number. Do you have the correct term?');
							}
						}).catch(function (error) { console.error(error);});
				};

				$scope.classNumberAddToCart = function() {
					vm.showSectionPane = false;
					vm.showCoursePane = false;
					vm.courseDetails = {};
					vm.courseSections = $scope.selectedPackage ? [$scope.selectedPackage] : $scope.packageData;
					vm.courseDetails.termCode = $scope.classData.classUniqueId.termCode;
					vm.courseDetails.subject = $scope.classData.subject;
					vm.courseDetails.courseId = $scope.classData.courseId;
					vm.courseDetails.catalogNumber = $scope.classData.catalogNumber;
					vm.courseDetails.credits = $scope.selectedCredits ? $scope.selectedCredits : vm.courseSections[0]._source.creditRange;
					vm.addToCart($scope.classNumber);
					$mdDialog.hide();
				};
			}
		}


		////////////////////
		// SEARCH RESULTS //
		////////////////////
		/**
		 * Check to make sure the search results are not empty

		 * @returns {Boolean} True if the results have data, false if not
		 */
		function resultsExist() {
			return (vm.searchResults.items.length > 0);
		}

		////////////////
		// PRE-SEARCH //
		////////////////
		function buildFilters() {
			vm.appliedFilters = [];

			// Clear existing filters
			filterService.clearFilters();

			// Add subject filter
			if ((vm.formData.subjectCode) && (vm.formData.subjectCode !== '0000')) {
				filterService.addSimpleString('subject.subjectCode', vm.formData.subjectCode, 'subject-filter');
			}

			if (vm.formData.seatsOpen || vm.formData.seatsWaitlisted || vm.formData.seatsClosed) {
				var seatsString = [];

				if (vm.formData.selectedTerm !== '0000') {
					if (vm.formData.seatsOpen) {
						seatsString.push('OPEN');
						vm.appliedFilters.indexOf('seats_OPEN') === -1 ? vm.appliedFilters.push('seats_OPEN') : '';
					}
					if (vm.formData.seatsWaitlisted) {
						seatsString.push('WAITLISTED');
						vm.appliedFilters.indexOf('seats_WAITLISTED') === -1 ? vm.appliedFilters.push('seats_WAITLISTED') : '';
					}
					if (vm.formData.seatsClosed) {
						seatsString.push('CLOSED');
						vm.appliedFilters.indexOf('seats_CLOSED') === -1 ? vm.appliedFilters.push('seats_CLOSED') : '';
					}

					filterService.addOpenSeats(seatsString.join(' '));
				}
			}

			// Add general education filter
			if ((vm.formData.generalEd.length) || (vm.formData.ethnicStudies)) {
				var genEdValues = [];
				for (var req in vm.formData.generalEd) {
					if (vm.formData.generalEd[req].value === true) {
						genEdValues.push(vm.formData.generalEd[req].key);
					}
				}
				if (genEdValues.length > 0) {
					var comB = (genEdValues.indexOf('COM B') !== -1) ? true : false;
					filterService.addGenEd(genEdValues, vm.formData.ethnicStudies, comB);
				}
				if (vm.formData.ethnicStudies && genEdValues.length === 0) {
					filterService.addSimpleString('ethnicStudies.code', 'ETHNIC ST', 'ethnic-studies');
				}
			}

			// Add breadth filter
			var breadths = [];
			for (var breadth in vm.formData.breadths) {
				if (vm.formData.breadths[breadth].value === true) {
					breadths.push(vm.formData.breadths[breadth].key);
				}
			}
			if (breadths.length > 0) {
				filterService.addSimpleArray('breadths.code', breadths, 'breadth-filter');
			}

			// Add level filter
			var levels = [];
			for (var level in vm.formData.levels) {
				if (vm.formData.levels[level].value === true) {
					levels.push(vm.formData.levels[level].key);
				}
			}
			if (levels.length > 0) {
				filterService.addSimpleArray('levels.code', levels, 'level-filter');
			}

			// Add honors filter
			if (vm.formData.honorsOnly || vm.formData.honorsApproved || vm.formData.honorsLevel) {
				var honorsString = [];

				if (vm.formData.honorsOnly) {
					honorsString.push('HONORS_ONLY');
				}
				if (vm.formData.honorsLevel) {
					honorsString.push('HONORS_LEVEL');
				}
				if (vm.formData.honorsApproved) {
					honorsString.push('INSTRUCTOR_APPROVED');
				}
				vm.appliedFilters.indexOf('honors_honors') === -1 ? vm.appliedFilters.push('honors_honors') : '';

				filterService.addHonors(honorsString);
			}

			// Add session filter
			if (vm.formData.session !== '') {
				filterService.addSession(vm.formData.session);
			}

			if (vm.formData.workplaceExperience) {
				filterService.addWorkforce();
			}

			if (vm.formData.foreignLanguage !== '') {
				filterService.addForeignLanguage(vm.formData.foreignLanguage);
			}

			if (vm.formData.serviceLearning) {
				filterService.addSpecialGroups('25 PLUS');
			}

			// Add reserved section formally special groups filter
			if ((vm.formData.specialGroup !== '') && (vm.formData.selectedSpecialGroupCategory !== '')) {
				filterService.addSpecialGroups(vm.formData.specialGroup);
				vm.appliedFilters.indexOf('reserved_reserved') === -1 ? vm.appliedFilters.push('reserved_reserved') : '';
			}

			// Add credit range filters
			if (vm.formData.creditsMin !== '') {
				filterService.addMinCredits(Number(vm.formData.creditsMin));
			}
			if (vm.formData.creditsMax !== '') {
				filterService.addMaxCredits(Number(vm.formData.creditsMax));
			}

			if (vm.formData.modeOfInstruction !== '') {
				filterService.addModeOfInstruction(vm.formData.modeOfInstruction);
			}

			// Add online only filter
			if (vm.formData.onlineOnly) {
				filterService.addSimpleString('onlineOnly', vm.formData.onlineOnly, 'online-only-filter');
			}

			// Add 50% grad coursework filter
			if (vm.formData.gradCourseWork) {
				filterService.addSimpleString('gradCourseWork', vm.formData.gradCourseWork, 'grad-coursework-filter');
			}

			// Add day filters
			// TODO: Add time filters (first we need to agree on the ranges of times offered)
			var days = [];
			for (var day in vm.formData.days) {
				if (vm.formData.days[day].value === true) {
					days.push(vm.formData.days[day].name);
				}
			}
			if (days.length > 0) {
				filterService.addDayTime(days);
			}

			// Return combined list of filters
			return filterService.buildFilterList();
		}

		function addToCart(enrollmentClassNumber, settings) {
			var termCode = vm.courseDetails.termCode,
				subjectCode = vm.courseDetails.subject.subjectCode,
				courseId = vm.courseDetails.courseId,
				courseName = vm.courseDetails.subject.shortDescription + ' ' + vm.courseDetails.catalogNumber,
				credits = (vm.courseDetails.maximumCredits === vm.courseDetails.minimumCredits) ? vm.courseDetails.minimumCredits : 0,
				classNumber = (enrollmentClassNumber) ? enrollmentClassNumber : '',
				data = {};
				vm.fromDegreePlan = angular.isUndefined(settings) ? false : true;

			// These properties are bound to the UI and the discussion for example gets checked.
			function setEnrollmentOptionsPostData(section) {
				if (section.classPermissionNumber) data.classPermissionNumber = section.classPermissionNumber;
				if (section.honorsChecked == true) data.honors = true;
				if (section.waitlistChecked == true) data.waitlist = true;
				data.credits = (section.credits) ? section.credits : credits;
			}

			// The lecture section of a package has the related class number property because we compare the lecture to the related class
			// number which is typically a lecture
			function setRelatedClassNumberPostData(section) {
				if (section.enrollmentOptions.relatedClassNumber) {
					data.relatedClassNumber1 = section.enrollmentOptions.relatedClasses[0];
					if (section.enrollmentOptions.relatedClasses.length == 2) data.relatedClassNumber2 = section.enrollmentOptions.relatedClasses[1];
				}
			}

			if (!enrollmentClassNumber) {
				// Set the classNumber for enrollment only if they have selected a section
				if (vm.hasPackages) {
					vm.courseSections.filter(function(section) {
						for (var i = section.packages.length - 1; i >= 0; i--) {
							if (section.packages[i].checked === true) {
								classNumber = section.packages[i][0].enrollmentClassNumber.toString();
								if (section.packages[i].length > 1) {
									setRelatedClassNumberPostData(section.packages[i][0]);
									setEnrollmentOptionsPostData(section.packages[i][1]);
								} else {
									setEnrollmentOptionsPostData(section.packages[i][0]);
									setRelatedClassNumberPostData(section.packages[i][0]);
								}
								break;
							}
						}
					});
				} else {
					for (var i = vm.courseSections.length - 1; i >= 0; i--) {
						if (vm.courseSections[i].checked === true) {
							classNumber = vm.courseSections[i].classUniqueId.classNumber.toString();
							setEnrollmentOptionsPostData(vm.courseSections[i]);
							break;
						}
					}
				}

			} else {
				data = { credits: vm.courseDetails.credits };

				if (vm.courseSections[0]._source.enrollmentOptions.relatedClassNumber) {
					setRelatedClassNumberPostData(vm.courseSections[0]._source);
				}
			}

			if (vm.DEBUG)console.info('addToCart post data:', termCode, subjectCode, courseId, classNumber, data);

			return cartService.addToCart(termCode, subjectCode, courseId, classNumber, data)
				.then(function(data) {
					if (data.error && data.error.status == 500) {
						toastService.infoMessage(courseName + ' Cannot add to cart: Server error');
					} else {
						$scope.$emit('cart-action', [vm.formData.selectedTerm, 'add-to-cart']);
						showToastAddToCart(courseName);
					}
				}).catch(function(error){ console.error(error) });
		}

		function showToastAddToCart(course) {
			if (isToastOpen) {
				$mdToast
					.hide()
					.then(function() {
						isToastOpen = false;
						$mdToast.show({
							position: 'bottom center',
							controller: ToastController,
							templateUrl: 'components/add-to-cart-toast/add-to-cart-toast.html',
							locals: {
								course: course,
								degreePlan: vm.fromDegreePlan
							},
							hideDelay: 10000
						});
					});
			} else {
				$mdToast.show({
					position: 'bottom center',
					controller: ToastController,
					templateUrl: 'components/add-to-cart-toast/add-to-cart-toast.html',
					locals: {
						course: course,
						degreePlan: vm.fromDegreePlan
					},
					hideDelay: 10000
				});
				isToastOpen = true;
			}

			function ToastController($scope, $location, $timeout, course, degreePlan, $mdToast) {
				$scope.courseName = course;
				$scope.degreePlan = degreePlan;
				$scope.dismiss = function() {
					$mdToast
						.hide()
						.then(function() {
							isToastOpen = false;
						});
				};
				$scope.goToCart = function() {
					$scope.dismiss();
					$location.search('q', null);
					$location.search('term', null);
					$location.search('subject', null);

					$location.path('/my-courses');
				};
			}
		}

		function addToSavedLater() {
			var subjectCode = vm.courseDetails.subject.subjectCode,
				courseName = vm.courseDetails.subject.shortDescription + ' ' + vm.courseDetails.catalogNumber,
				courseId = vm.courseDetails.courseId;

			return cartService.addToFavorites(subjectCode, courseId)
				.then(function success(data) {
					if (data && data.error.status == 500) {
						toastService.infoMessage(courseName + ' Cannot add to saved for later server error');
					} else {
						var toast = $mdToast.simple()
								.textContent(courseName + ' saved for later')
								.action('Go to cart')
								.position('bottom center')
								.hideDelay(5000)
								.highlightAction(false);

						$mdToast.show(toast).then(function(response) {
							if (response == 'ok') {
								$location.path('/my-courses');
							}
						}, function() {});

						// REFACTOR TO USE SERVICE. MATERIAL DOCUMENTATION WAS PRETTY SPARSE MAY NEED TO EVALUATE SOURCE
						// toastService.messageWithAction(myToast);
					}

				});
		}

		function setReservedSections() {
			if (vm.selectedSpecialGroupCategory === 0) vm.formData.specialGroup = '';
		}

		function toggleSidenavSection() {
			$mdSidenav('right', true).onClose(function() {
				vm.courseSections = [];
			});

			if ($mdSidenav('right').isOpen() == false) {
				vm.loadingSections = true;
				$mdSidenav('right').toggle().then(function() {
					return getCourseSections(vm.courseDetails.courseId, vm.courseDetails.subject.subjectCode, vm.appliedFilters).then(function() {
						vm.loadingSections = false;
					});
				});
			} else {
				$mdSidenav('right').toggle();
			}
		}

		function addToDegreePlan() {
			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: event,
				templateUrl: 'components/dialogs/add-to-degree-plan-dialog.html',
				locals: {
					degreePlans: vm.degreePlans,
					activeTerms: vm.terms,
					course: vm.courseDetails
				},
				clickOutsideToClose: true,
				controller: AddToPlanController
			});

			function AddToPlanController($scope, $mdDialog, course, degreePlans, activeTerms, cartService, toastService, $location) {
				$scope.degreePlans = degreePlans;
				$scope.course = course;
				$scope.activeTerms = activeTerms;
				$scope.degreePlanTerms = {};

				$scope.closeDialog = function() {
					$mdDialog.hide();
				}
				$scope.planChange = function() {
					cartService.getDegreePlanTerms($scope.selectedPlan.roadmapId).then(function(data) {
						$scope.selectedTerm = {};
						$scope.degreePlanTerms = data;
					}).catch(function(error) { console.error(error); });
				}
				$scope.addToPlan = function() {
					var data = {
						subjectCode: $scope.course.subject.subjectCode,
						courseId: $scope.course.courseId,
						termCode: $scope.selectedTerm
					};

					if ($scope.selectedPlan.primary && $scope.activeTerms.some(function(term) { return term.termCode === $scope.selectedTerm })) {
						$mdDialog.hide();
						vm.addToCart(null, {degreePlan:true });
					} else {
						cartService.addToDegreePlan($scope.selectedPlan.roadmapId, data).then(function() {
							$mdDialog.hide();
							var message = $scope.course.subject.shortDescription + ' ' + $scope.course.catalogNumber +
											' added to ' + $scope.selectedPlan.name + ' degree plan';
							$mdToast.show({
								position: 'bottom center',
								controller: ToastController,
								template :  '<md-toast role="alert" aria-relevant="all">' +
												'<span class="md-toast-text" flex>{{ message }}</span>' +
												'<md-button md-no-ink href="/degree-planner" target="_self">GO TO PLAN</md-button>' +
											'</md-toast>',
								locals: {
									message: message
								},
							});
							function ToastController($scope, message) {
								$scope.message = message;
							}
						}).catch(function(error) { console.error(error); });
					}
				}
			}
		}
	}

})();
