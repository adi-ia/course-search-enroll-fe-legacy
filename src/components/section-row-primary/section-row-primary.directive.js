(function() {
	'use strict';
	/**
	@desc directive for displaying a parent section (LEC). Does not have an isolate scope.
	@example class="section-row-primary"
	*/
	angular
		.module('app.core')
		.directive('sectionRowPrimary', sectionRowPrimary);

	function sectionRowPrimary($mdSticky) {
		return {
			templateUrl: 'components/section-row-primary/section-row-primary.html',
			restrict: 'C',
			transclude: true,
			link: function($scope, element) {
				var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
				if (!isIE11 && element[0].parentElement.classList[0].indexOf('your-selected-sections') == -1)
				$mdSticky($scope, element);
			}
		};
	}

})();
