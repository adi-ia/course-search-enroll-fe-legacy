(function() {
	'use strict';
	/**
	@desc directive for displaying course details, including instructor, meeting times, etc. This is used by
			both the search view and the my-courses view.
	@example <section-details course=""></section-details>
	*/
	angular
		.module('app.core')
		.directive('sectionDetails', sectionDetails)
		.filter('formatChipName', function() {
			return function(chip) {
				switch (chip) {
					case 'seats_OPEN':
						return 'Open seats';
					case 'seats_WAITLISTED':
						return 'Wait listed seats';
					case 'seats_CLOSED':
						return 'Closed seats';
					case 'honors_honors':
						return 'Honors';
					case 'reserved_reserved':
						return 'Reserved section';
					default:
						return 'something went wrong';
				}
			};
		})
		.filter('honorsFilter', function() {
			return function(honors) {
				switch (honors) {
					case 'HONORS_ONLY':
						return 'Honors only';
					case 'HONORS_LEVEL':
						return 'Accelerated honors';
					case 'INSTRUCTOR_APPROVED':
						return 'Honors optional';
				}
			};
		});

	function sectionDetails() {
		return {
			templateUrl: 'components/section-details/section-details.html',
			restrict: 'E',
			scope: {
				parentPackageType: '=',
				term: '=?'
			},
			link: function($scope, $element) {
				if ($element[0].id == 'section-details') {
					$scope.$on("polling-done", function ($event) {
						if (!$scope.vm.isLocked) {
							$scope.vm.hideSelectedSection = true;
							$scope.vm.selectedSection = false;
							$scope.$parent.vm.hideSections = false;
						} else {
							$scope.vm.hideSelectedSection = false;
						}
					});
				}
			},
			controller: SectionDetailsController,
			controllerAs: 'vm',
			bindToController: {
				hasPackages: '=',
				hasOptionalSection: '=',
				course: '=',
				sections: '=',
				selectedSection: '=?',
				sectionMaxLength: '=?',
				hideSelectedSection: '=?',
				cart: '=?',
				onScheduler: '=?',
				disableLockButton: '=?',
				onSearch: '=?',
				onMyCourses: '=?',
				sharedAddToCart: '&',
				appliedFilters: '=',
				hasPendingValidation: '=?',
				loadingSections: '=?',
				loadTempCart: '=?',
				sessions: '=?',
				selectAllOrNothing: '=?',
				sectionMulti: '=?'
			}
		};
	}

	/* @ngInject */
	function SectionDetailsController(constants, $scope, $rootScope, $timeout, formatService, cartService, sharedService, $q, $mdDialog, enrollmentService) {
		var vm = this;
		vm.DEBUG						= constants.DEBUG;

		/////////////////////
		// EXPOSED METHODS //
		/////////////////////
		vm.hasClassMaterials			= hasClassMaterials;
		vm.hasExamDate					= hasExamDate;
		vm.checkedPrimary				= checkedPrimary;
		vm.checkedSecondary				= checkedSecondary;
		vm.getEnrollmentOptions			= getEnrollmentOptions;
		vm.createCreditRange			= createCreditRange;
		vm.updateCart					= updateCart;
		vm.updateCartPackages			= updateCartPackages;
		vm.showFootnotes				= showFootnotes;
		vm.showTextbooks				= showTextbooks;
		vm.showInstructors				= showInstructors;
		vm.showInstructorContent		= showInstructorContent;
		vm.showDeadlines			    = showDeadlines;
		vm.showReservedSections			= showReservedSections;
		vm.removeFilter					= removeFilter;
		vm.saveSection					= saveSection;
		vm.sectionPrimaryClick			= sectionPrimaryClick;
		vm.updatingEnrolled 			= false;
		vm.selectAllOrNothing			= false;
		vm.updatingEnrolledErrors		= [];
		vm.multiResetSelection			= multiResetSelection;
		// shared functions
		vm.hasEnrollmentOptions			= hasEnrollmentOptions;

		function removeFilter($index) {
			vm.appliedFilters.splice($index, 1);
		}

		function hasClassMaterials() {
			var materialsDefined = false;
			var hasSections = (vm.sections[0] !== undefined) ? true : false;
			if (hasSections) {
				for (var i = hasSections - 1; i >= 0; i--) {
					materialsDefined = vm.sections[i].classMaterials.some(function(value) { return value.textbooks.length > 0; });
					if (materialsDefined) {
						break;
					}
				}
			}
			return materialsDefined;
		}

		function hasExamDate(section) {
			return section.classMeetings.some(function(value) { return value.meetingType == 'EXAM'; });
		}

		function checkedPrimary(selectedSection, index, settings) {
			// *Note: modifies vm.courseSections from directive as 'sections'.
			if (selectedSection.checked) {
				vm.currentSelectedIndex = index;
				vm.currentSelectClassNumber = selectedSection.classUniqueId.classNumber;
			}

			if (vm.sections && vm.sections.length) {
				// Deselect all checkboxs before selecting the selected one.
				vm.sections.map(function(section) {
					section.checked = false;
				});

				if (vm.hasOptionalSection) {
					vm.sections.map(function(section) {
						section.packages.map(function(singlePackage) {
							singlePackage.map(function(packageSection) { packageSection.checked = false; });
							singlePackage.checked = false;
							// vm.selectedSection = false;
						});
					});
				}
			}
			// Adds the ability to uncheck checkboxes, edge case if same index on two different lectures is selected then we resort to matching class number
			// if ((vm.currentSelectedIndex == index) && (settings) && (settings.locked)) {
			// 	vm.currentSelectedIndex = -1;
			// 	// vm.selectedSection = false;
			// 	return;
			// }
			if (!vm.isLocked && vm.selectedSection) {
				vm.selectedSection = false;
				return;
			}

			var selectedSectionClone = angular.copy(selectedSection);
			selectedSection.checked = true;
			selectedSectionClone.show = true;
			selectedSectionClone.checked = true;
			vm.selectedSection = [selectedSectionClone];
		}

		function checkedSecondary(selectedPackage, index, settings) {
			if (selectedPackage.checked) {
				vm.currentSelectedIndex = index;
				vm.currentSelectClassNumber = selectedPackage[0].enrollmentClassNumber;
			}

			// Deselect all checkboxs before selecting the selected one. When checking a DIS with a LEC we need to check the DIS so we add a checked property to
			// each package class (LEC,DIS etc) and the package as a whole which makes it easier to find when we add to cart.
			// Deselect all checkboxs before selecting the selected one.
			vm.sections.map(function(section) {
				section.packages.map(function(singlePackage) {
					singlePackage.map(function(packageSection) { packageSection.checked = false; });
					singlePackage.checked = false;
				});
			});

			// Bail out because we need to unlock the current package
			if (!vm.isLocked && vm.selectedSection) {
				vm.selectedSection = false;
				return;
			}

			// Adds the ability to uncheck checkboxes, edge case if same index on two different lectures is selected then we resort to matching class number
			if ((vm.hasOptionalSection) && (selectedPackage.length == 1)) {
				if ((vm.currentSelectedIndex == index) && (selectedPackage[0].classUniqueId.classNumber == vm.currentSelectClassNumber)) {
					vm.currentSelectedIndex = -1;
					// vm.selectedSection = false;
					return;
				}
			} else {
				// if ((vm.currentSelectedIndex == index) && (settings) && (!settings.locked)) {
				// 	vm.currentSelectedIndex = -1;
				// 	// vm.selectedSection = false;
				// 	return;
				// }
			}

			// If package consists of more than 1 class (Lec,Lab,Dis) set checked to true for all of them.
			selectedPackage.map(function(singleSection) { singleSection.checked = true; });
			vm.sectionMaxLength.map(function(singleSection) { singleSection.notSelected = false; });
			selectedPackage.checked = true;

			vm.currentSelectedIndex = index;
			vm.currentSelectClassNumber = (selectedPackage.length == 1) ? selectedPackage[0].enrollmentClassNumber : selectedPackage[1].classUniqueId.classNumber;

			if (vm.isLocked) {
				var selectedPackageClone = angular.copy(selectedPackage);
				selectedPackageClone.map(function(section) { section.showPackage = true; section.checked = true;});
				vm.sections.map(function(lecture) { return lecture.packages.map(function(pack) { if(pack.checked) lecture.checked=true; }); });
				var parent = selectedPackageClone[0];
				parent.packages = [selectedPackageClone];
				vm.selectedSection = [parent];
			}
		}

		function getCheckedPackage() {
			var selectedIndex = '';
			for (var i = vm.sections.length - 1; i >= 0; i--) {
				for (var j = vm.sections[i].packages.length - 1; j >= 0; j--) {
					if (!vm.hasOptionalSection) {
						if (vm.sections[i].packages[j][1].checked == true) {
							selectedIndex = { lecIndex: i, pacIndex: j};
							return selectedIndex;
						}
					} else {
						if (vm.sections[i].checked == true) {
							selectedIndex = { lecIndex: i, pacIndex: 0};
							return selectedIndex;
						}
					}

				}
			}
		}

		function getEnrollmentOptions(selectedPackage) {
			// if we are on the search page we bypass this
			selectedPackage = selectedPackage || '';
			if ((!selectedPackage) && (vm.cart)) {
				var checkedPackage = getCheckedPackage();
				if (checkedPackage) {
					selectedPackage = vm.sections[checkedPackage.lecIndex].packages[checkedPackage.pacIndex];
				}
			}

			if ((selectedPackage[0] !== undefined) && (vm.hasPackages)) {
				if ((vm.hasOptionalSection && selectedPackage.length == 1) && (selectedPackage[0].creditRange) && (vm.course.creditRange != selectedPackage[0].creditRange)) {
					selectedPackage.variableCreditsException = true;
					selectedPackage[0].credits = Number(selectedPackage[0].creditRange);
					if (vm.DEBUG)console.info('Set credits to', selectedPackage[0].creditRange);
				} else if ((selectedPackage[1]) && (selectedPackage[1].creditRange) && (vm.course.creditRange != selectedPackage[1].creditRange)) {
					selectedPackage.variableCreditsException = true;
					selectedPackage[1].credits = Number(selectedPackage[1].creditRange);
					if (vm.DEBUG)console.info('Set credits to', selectedPackage[1].creditRange);
				}
			} else if (selectedPackage) {
				if (selectedPackage.creditRange.indexOf('-') === -1) {
					selectedPackage.variableCreditsException = true;
					selectedPackage.credits = Number(selectedPackage.creditRange);
					if (vm.DEBUG)console.info('Set credits to', selectedPackage.creditRange);
				}
			}
		}

		function createCreditRange(section) {
			function range(start, stop) {
				var a = [start], b = start;
				while (b < stop) { b += 1; a.push(b); }
				return a;
			}
			if (section.creditRange.indexOf('-') === -1) {
				return;
			} else {
				var min = Number(section.creditRange.split('-')[0]),
					max = Number(section.creditRange.split('-')[1]);

				return range(min, max);
			}
		}

		function updateCart(section, $id, settings) {
			// This handles user actions when interacting with a selected section
			if (vm.selectedSection && angular.equals(section, vm.selectedSection[0])) {
				vm.getEnrollmentOptions(section);
				vm.hideSections = false;
				if (vm.isLocked) {
					if (!vm.onScheduler) {
						vm.sections.forEach(function(singleSection, index) {
							if (vm.sections[index].checked) {
								vm.sections[index].honorsChecked = vm.selectedSection[0].honorsChecked;
								vm.sections[index].waitlistChecked = vm.selectedSection[0].waitlistChecked;
								vm.sections[index].classPermissionNumber = vm.selectedSection[0].classPermissionNumber;
								vm.sections[index].credits = vm.selectedSection[0].credits;
							}
						});
					} else {
						vm.sections.map(function(section) {
							if (section.classUniqueId.classNumber == vm.selectedSection[0].enrollmentClassNumber) {
								section.checked = true;
								vm.selectedSection.checked = true;
								section.honorsChecked = vm.selectedSection[0].honorsChecked;
								section.waitlistChecked = vm.selectedSection[0].waitlistChecked;
								section.classPermissionNumber = vm.selectedSection[0].classPermissionNumber;
								section.credits = vm.selectedSection[0].credits;
								if (vm.isLocked && angular.isUndefined(settings.locked)) {
									$rootScope.$broadcast('run-generate');
								} else {
									vm.loadTempCart = true;
								}
							}
						});
					}
				} else {
					vm.sections.map(function(section) { section.checked = false; });
					if (vm.hasOptionalSection) {
						vm.sections.map(function(section) {
							section.packages.map(function(singlePackage) {
								singlePackage.map(function(packageSection) { packageSection.checked = false; });
								singlePackage.checked = false;
								section.checked = false;
							});
						});
					}
				}
				return vm.sharedAddToCart();
			}
			vm.checkedPrimary(section, $id, settings);
			vm.getEnrollmentOptions(section);
			vm.sharedAddToCart();
		}

		function updateCartPackages(sections, $id, settings) {
			// This prevents a user from interacting with the UI before sections are loaded.
			// if (vm.loadingSections) { return $timeout(function() { updateCartPackages(sections, $id, settings); }, 1000); }

			// This handles user actions when interacting with a selected section
			if (vm.selectedSection && angular.equals(sections, vm.selectedSection[0].packages[0])) {
				vm.getEnrollmentOptions(sections);
				vm.hideSections = false;
				if (vm.isLocked || angular.isUndefined(vm.isLocked)) {
					if (!vm.onScheduler) {
						vm.sections.forEach(function(section) {
							section.packages.forEach(function(singlePackage) {
								if (singlePackage.checked) {
									singlePackage[1].honorsChecked = vm.selectedSection[0].packages[0][1].honorsChecked || false;
									singlePackage[1].waitlistChecked = vm.selectedSection[0].packages[0][1].waitlistChecked || false;
									singlePackage[1].classPermissionNumber = vm.selectedSection[0].packages[0][1].classPermissionNumber;
									singlePackage[1].credits = vm.selectedSection[0].packages[0][1].credits;
								}
							});
						});
					} else {
						vm.sections.forEach(function(course) {
							course.packages.forEach(function(onePackage) {
								if (onePackage[0].enrollmentClassNumber == vm.selectedSection[0].enrollmentClassNumber) {
									onePackage[onePackage.length-1].honorsChecked = vm.selectedSection[0].packages[0][1].honorsChecked || false;
									onePackage[onePackage.length-1].waitlistChecked = vm.selectedSection[0].packages[0][1].waitlistChecked || false;
									onePackage[onePackage.length-1].classPermissionNumber = vm.selectedSection[0].packages[0][1].classPermissionNumber;
									onePackage[onePackage.length-1].credits = vm.selectedSection[0].packages[0][1].credits;
									onePackage.checked = true;
									vm.selectedSection[0].checked = true;
									vm.selectedSection[0].sections[1].checked = true;
									course.checked = true;
									if (vm.isLocked && angular.isUndefined(settings.locked)) {
										$rootScope.$broadcast('run-generate');
									} else {
										vm.loadTempCart = true;
									}
								}
							});
						});
					}
				} else {
					vm.sections.map(function(section) {
						section.packages.map(function(singlePackage) {
							singlePackage.map(function(packageSection) { packageSection.checked = false; });
							singlePackage.checked = false;
							section.checked = false;
						});
					});
				}
				return vm.sharedAddToCart();
			}
			vm.checkedSecondary(sections, $id, settings);
			vm.getEnrollmentOptions(sections);
			vm.sharedAddToCart();
		}

		function saveSection(section, packages, $id, $event, settings, change) {
			vm.disableLockButton = true;
			vm.isLocked = (angular.isUndefined(settings.locked)) ? true : settings.locked;
			vm.selectedId = $id;
			if (!vm.isLocked && !vm.selectedSection) vm.hideSelectedSection = true;
			if ($event && $event.currentTarget.attributes.class.nodeValue.match(/(ghost-button|md-raised)/)) {
				if (vm.DEBUG) console.log('$broacast generate');
				$rootScope.$broadcast('run-generate');
			}

			if(section.enrolled){
				confirmEnrolledChangesDialog(section, change);
			} else if (packages) {
				settings.packageSize = packages.length;
				var hasReservedSection = packages.filter(function(section) { return (section.classAttributes.length && (['FIG', 'RESH', 'TRFR', 'WES'].indexOf(section.classAttributes[0].attributeCode) >= 0));})[0];
				if (hasReservedSection) {
					$mdDialog.show({
						parent: angular.element(document.body),
						targetEvent: event,
						fullscreen: true,
						template: '<md-dialog flex="50" aria-label="section reserved-sections">' +
									'<md-toolbar class="md-primary">' +
										'<div class="md-toolbar-tools">Reserved section</div>' +
									'</md-toolbar>' +
									'<md-dialog-content class="md-dialog-content">' +
										'<div layout="row" layout-align="center center" class="layout-align-center-center layout-row">' +
											'<div flex="100" flex-100">' +
												'<div class="text-center"><img flex="30" src="img/bucky-shrug.svg" alt="shrug bucky image"></div>' +
												'<p>This section is reserved. You will need to be a part of one of the following reserved groups in order to enroll. Would you still like to lock this section?</p>' +
												'<div ng-repeat="displayName in hasReservedSection.classAttributes | unique:&quot;displayName.attributeDisplayName&quot;" class="semi-bold">' +
													'{{ displayName.attributeDisplayName }}' +
												'</div>' +
												'<div ng-repeat="message in hasReservedSection.classAttributes">{{ message.valueDescription }}</div>' +
											'</div>' +
										'</div>' +
									'</md-dialog-content>' +
									'<md-dialog-actions>' +
										'<md-button ng-click="closeDialog()" class="md-primary md-button" aria-label="close dialog">Close</md-button>' +
										'<md-button ng-click="updateCartPackages(packages, selectedId, settings); closeDialog();" class="md-primary md-raised md-button" aria-label="proceed action">Proceed</md-button>' +
									'</md-dialog-actions>' +
								'</md-dialog>',
						locals: {
							hasReservedSection: hasReservedSection,
							section: null,
							selectedId: vm.selectedId,
							settings: settings,
							packages: packages
						},
						clickOutsideToClose: true,
						multiple: true,
						controller: SaveSectionController
					});
				} else {
					updateCartPackages(packages, vm.selectedId, settings);
				}
			} else {
				settings.packageSize = 1;
				if (section.classAttributes.length > 0 && section.classAttributes.some(function(attrib) { return ['TEXT','SVCL'].indexOf(attrib.attributeCode) == -1})) {
					$mdDialog.show({
						parent: angular.element(document.body),
						targetEvent: event,
						fullscreen: true,
						template: '<md-dialog flex="50" aria-label="section reserved-sections">' +
									'<md-toolbar class="md-primary">' +
										'<div class="md-toolbar-tools">Reserved section</div>' +
									'</md-toolbar>' +
									'<md-dialog-content class="md-dialog-content">' +
										'<div layout="row" layout-align="center center" class="layout-align-center-center layout-row">' +
											'<div flex="100" flex-100">' +
												'<div class="text-center"><img flex="30" src="img/bucky-shrug.svg" alt="shrug bucky image"></div>' +
												'<p>This section is reserved. You will need to be a part of one of the following reserved groups in order to enroll. Would you still like to lock this section?</p>' +
													'<div ng-repeat="displayName in section.classAttributes | unique:&quot;displayName.attributeDisplayName&quot;" class="semi-bold">' +
														'{{ displayName.attributeDisplayName }}' +
													'</div>' +
													'<div ng-repeat="message in section.classAttributes">{{ message.valueDescription }}</div>' +
											'</div>' +
										'</div>' +
									'</md-dialog-content>' +
									'<md-dialog-actions>' +
										'<md-button ng-click="section.checked = false; closeDialog()" class="md-primary md-button" aria-label="close dialog">Close</md-button>' +
										'<md-button ng-click="updateCart(section, selectedId, settings); closeDialog();" class="md-primary md-raised md-button" aria-label="proceed action">Proceed</md-button>' +
									'</md-dialog-actions>' +
								'</md-dialog>',
						locals: {
							hasReservedSection: null,
							section: section,
							selectedId: vm.selectedId,
							settings: settings,
							packages: null
						},
						clickOutsideToClose: true,
						multiple: true,
						controller: SaveSectionController
					});
				} else {
					updateCart(section, vm.selectedId, settings);
				}
			}

			function confirmEnrolledChangesDialog(section, change) {

				// Build the object of all the data we need to update this course
				// And the data needed to undo those changes if the users choses to do so
				var updateData = {
					termCode: vm.course.termCode,
					enrollmentClassNumber: vm.course.enrollmentClassNumber,
					relatedClassNumber1: vm.course.enrollmentOptions.relatedClasses[0],
					relatedClassNumber2: vm.course.enrollmentOptions.relatedClasses[1],
					classPermissionNumber: vm.course.classPermissionNumber,
					waitlist: (vm.course.studentEnrollmentStatus == 'W') ? true : false,
					from: {
						credits: parseInt(vm.course.credits),
						honors: vm.course.honors
					},
					to: {
						credits: parseInt(section.credits ? section.credits : vm.course.credits),
						honors: section.honorsChecked
					},
					section: section,
					course: vm.course,
					change: change,
					vm: vm
				};

				$mdDialog.show({
					parent: angular.element(document.body),
					templateUrl: 'components/dialogs/confirm-enrolled-changes.html',
					clickOutsideToClose: false,
					multiple: true,
					scope: $scope,
					preserveScope: true,
					fullscreen: true,
					controller: enrolledChangesController,
					locals: {updateData: updateData}
				})
				// Then on success
				.then(function(){
					enrollmentService.updateEnrolledCourse(updateData);
				})
				// Catch on cancel
				.catch(function(){
					switch(change){
						case 'credits':
							updateData.section.credits = updateData.from.credits;
							break;

						case 'honors':
							updateData.section.honorsChecked = updateData.to.honors ? false : true;
							break;
					}
				});
	
				/* @ngInject */
				function enrolledChangesController($scope, $mdDialog, updateData) {

					$scope.change = updateData.change;

					// Generate the "to" and "from" text for the dialog
					switch(change){
						case 'credits':
							$scope.to = updateData.to.credits + " " + (updateData.to.credits == 1 ? 'Credit' : 'Credits');
							$scope.from = updateData.from.credits + " " + (updateData.to.credits == 1 ? 'Credit' : 'Credits');
							break;

						case 'honors':
							if(updateData.to.honors){
								$scope.to = 'Take this course with Honors';
								$scope.from = 'Do not take this course with Honors';
							} else {
								$scope.to = 'Do not take this course with Honors';
								$scope.from = 'Take this course with Honors';
							}
							break;
					}
	
					// On close without save, revert back to the previous selection
					$scope.closeDialog = function() {
						$mdDialog.cancel();
					};

					// If the user saves changes, make an API call to update this course
					$scope.saveChanges = function() {
						$mdDialog.hide();
					};
				}
			}


			function SaveSectionController($scope, $mdDialog, hasReservedSection, section, selectedId, settings, packages) {
				$scope.hasReservedSection = hasReservedSection;
				$scope.selectedId = selectedId;
				$scope.section = section;
				$scope.settings = settings;
				$scope.packages = packages;
				$scope.closeDialog = function() {
					$mdDialog.hide();
					vm.disableLockButton = false;
				};
				$scope.updateCartPackages = function (selectedSection, selectedId, settings) {
					settings.reserved = true;
					updateCartPackages(selectedSection, selectedId, settings);
				}
				$scope.updateCart = function (section, selectedId, settings) {
					updateCart(section, selectedId, settings);
				}
			}
		}

		function showFootnotes(event, section) {
			event.preventDefault();event.stopPropagation();
			var sectionName;
			if (section.type && section.sectionNumber) {
				sectionName = section.type + ' ' + section.sectionNumber;
			}
			var courseName = vm.course.subject.shortDescription + ' ' + vm.course.catalogNumber;
			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: event,
				templateUrl: 'components/section-footnotes-dialog/section-footnotes-dialog.html',
				fullscreen: true,
				locals: {
					course: courseName,
					section: sectionName,
					footnotes: section.footnotes
				},
				clickOutsideToClose: true,
				multiple: true,
				controller: FootnotesController
			});

			function FootnotesController($scope, $mdDialog, course, section, footnotes) {
				$scope.course = course;
				$scope.section = section;
				$scope.footnotes = footnotes;

				$scope.closeDialog = function() {
					$mdDialog.hide();
				};
			}
		}

		function showTextbooks(event, section) {
			event.preventDefault();event.stopPropagation();
			var sectionName;
			if (section.type && section.sectionNumber) {
				sectionName = section.type + ' ' + section.sectionNumber;
			}
			var courseName = vm.course.subject.shortDescription + ' ' + vm.course.catalogNumber;
			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: event,
				templateUrl: 'components/section-textbooks-dialog/section-textbooks-dialog.html',
				fullscreen: true,
				locals: {
					courseName: courseName,
					courseTitle: vm.course.title,
					section: section
				},
				clickOutsideToClose: true,
				multiple: true,
				controller: TextbookController
			});

			function TextbookController($scope, $mdDialog, courseName, courseTitle, section) {
				$scope.courseName = courseName;
				$scope.courseTitle = courseTitle;
				$scope.section = section;

				$scope.closeDialog = function() {
					$mdDialog.hide();
				};
			}
		}

		function showInstructorContent(event, section) {
			event.preventDefault();event.stopPropagation();
			var sectionName;
			if (section.type && section.sectionNumber) {
				sectionName = section.type + ' ' + section.sectionNumber;
			}
			var courseName = vm.course.subject.shortDescription + ' ' + vm.course.catalogNumber;
			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: event,
				templateUrl: 'components/section-instructor-content-dialog/section-instructor-content-dialog.html',
				fullscreen: true,
				locals: {
					courseName: courseName,
					courseTitle: vm.course.title,
					course: section
				},
				clickOutsideToClose: true,
				multiple: true,
				controller: InstructorController
			});

			function InstructorController($scope, $mdDialog, course, courseName, courseTitle) {
				$scope.courseName = courseName;
				$scope.course = course;
				$scope.courseTitle = courseTitle;

				$scope.closeDialog = function() {
					$mdDialog.hide();
				};
			}
		}
        function showDeadlines(event, section) {
            event.preventDefault();event.stopPropagation();
            var sectionName;
            var foundSession;
            if (section.type && section.sectionNumber) {
                sectionName = section.type + ' ' + section.sectionNumber;
            }
            var courseName = vm.course.subject.shortDescription + ' ' + vm.course.catalogNumber;
            vm.sessions.forEach(function(session) {
                if(session.sessionCode === section.sessionCode) {
                    foundSession = session;
                }
            });
            $mdDialog.show({
                parent: angular.element(document.body),
                targetEvent: event,
                templateUrl: 'components/dialogs/session-deadlines-dialog.html',
                fullscreen: true,
                locals: {
                    course: courseName,
                    section: sectionName,
                    sessionCode: section.sessionCode,
					session: foundSession
                },
                clickOutsideToClose: true,
                multiple: true,
                controller: DeadlinesController
            });

            function DeadlinesController($scope, $mdDialog, course, section, sessionCode, session) {
                $scope.course = course;
                $scope.section = section;
                $scope.sessionCode = sessionCode;
                $scope.session = session;


                $scope.closeDialog = function() {
                    $mdDialog.hide();
                };
            }
        }
		function showInstructors(event, section) {
			event.preventDefault();event.stopPropagation();
			var sectionName;
			if (section.type && section.sectionNumber) {
				sectionName = section.type + ' ' + section.sectionNumber;
			}
			var courseName = vm.course.subject.shortDescription + ' ' + vm.course.catalogNumber;
			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: event,
				fullscreen: true,
				template: '<md-dialog aria-label="section reserved-sections">' +
							'<md-toolbar class="md-primary">' +
								'<div class="md-toolbar-tools">{{ courseName }} - {{ sectionName }} - Instructors</div>' +
							'</md-toolbar>' +
							'<md-dialog-content class="md-dialog-content">' +
								'<div ng-repeat="instructor in course.instructors" style="margin: 15px 0">' +
									'<md-icon aria-hidden="true" class="material-icons" style="margin-right: 15px;" role="img">person</md-icon>' +
									'<span class="capitalize"> {{ instructor.name.first | lowercase }} {{ instructor.name.last | lowercase }}</span>' +
									'<span class="capitalize" ng-if="!instructor.name"> {{ instructor | lowercase }} </span>' +
									'<span ng-if="instructor.email">,</span> <a href="mailto:{{ instructor.email }}">{{instructor.email | lowercase}}</a>' +
								'</div>' +
							'</md-dialog-content>' +
							'<md-dialog-actions>' +
								'<md-button ng-click="closeDialog()" class="md-primary md-raised" aria-label="close dialog">Close</md-button>' +
							'</md-dialog-actions>' +
						'</md-dialog>',
				locals: {
					courseName: courseName,
					sectionName: sectionName,
					course: section
				},
				clickOutsideToClose: true,
				multiple: true,
				controller: InstructorController
			});

			function InstructorController($scope, $mdDialog, course, courseName, sectionName) {
				$scope.courseName = courseName;
				$scope.course = course;
				$scope.sectionName = sectionName;

				$scope.closeDialog = function() {
					$mdDialog.hide();
				};
			}
		}

		function showReservedSections(event, section) {
			event.preventDefault();event.stopPropagation();
			var sectionName;
			if (section.type && section.sectionNumber) {
				sectionName = section.type + ' ' + section.sectionNumber;
			}
			var courseName = vm.course.subject.shortDescription + ' ' + vm.course.catalogNumber;
			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: event,
				fullscreen: true,
				template: '<md-dialog aria-label="section reserved-sections">' +
							'<md-toolbar class="md-primary">' +
								'<div class="md-toolbar-tools">{{ courseName }}: {{ courseTitle }} - Reserved section</div>' +
							'</md-toolbar>' +
							'<md-dialog-content class="md-dialog-content">' +
								'<div ng-repeat="displayName in course.classAttributes | unique:&quot;displayName.attributeDisplayName&quot;" class="semi-bold">' +
									'{{ displayName.attributeDisplayName }}' +
								'</div>' +
								'<div ng-repeat="message in course.classAttributes">{{ message.valueDescription }}</div>' +
							'</md-dialog-content>' +
							'<md-dialog-actions>' +
								'<md-button ng-click="closeDialog()" class="md-primary" aria-label="close dialog">Got it!</md-button>' +
							'</md-dialog-actions>' +
						'</md-dialog>',
				locals: {
					courseName: courseName,
					courseTitle: sectionName,
					course: section
				},
				clickOutsideToClose: true,
				multiple: true,
				controller: ReservedController
			});

			function ReservedController($scope, $mdDialog, course, courseName, courseTitle) {
				$scope.courseName = courseName;
				$scope.course = course;
				$scope.courseTitle = courseTitle;

				$scope.closeDialog = function() {
					$mdDialog.hide();
				};
			}
		}

		function hasEnrollmentOptions(course) {
			return sharedService.hasEnrollmentOptions(course);
		}

		function sectionPrimaryClick(section) {
			section.show = !section.show;
		}

		function multiResetSelection() {
			$scope.vm.selectAllOrNothing = !$scope.vm.selectAllOrNothing;
			if ($scope.vm.hasPackages) {
				$scope.vm.sections.forEach(function(section) {
					section.packages.forEach(function(packages) {
						if (packages.length > 1) {
							packages[1].multiChecked = !$scope.vm.selectAllOrNothing;
						} else {
							packages[0].multiChecked = !$scope.vm.selectAllOrNothing;
						}
					});
				});
			} else {
				$scope.vm.sections.forEach(function(section) {
					section.multiChecked = !$scope.vm.selectAllOrNothing;
				});
			}
		}
	}
})();
