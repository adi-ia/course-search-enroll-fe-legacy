'use strict';

describe('Module: app.search', function() {

	beforeEach(module('app.core'));
	beforeEach(module('app.courses'));

	describe('Controller: courses.controller', function(){

		////////////////////
		//    CRITICAL    //
		////////////////////
		it('should have CoursesController', inject(function($controller) {
			//spec body
			var coursesController = $controller('CoursesController');
			expect(coursesController).toBeDefined();
		}));
		it('should initialize with an empty terms array and no term (or all terms) selected', inject(function($controller) {
			var coursesController = $controller('CoursesController');
			expect(coursesController.terms.length).toBe(0);
			expect(coursesController.selectedTerm).toBe('');
		}));

	});
});