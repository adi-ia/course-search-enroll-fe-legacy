(function() {
	'use strict';
	/**
	 * Service used to compare sets of cart data.
	 */
	angular
		.module('app.core')
		.factory('courseDigestService', courseDigestService);

	/* @ngInject */
	function courseDigestService() {
		var service = {
			getDigest:			getDigest,
			compareDigests:		compareDigests
		};
		return service;

		function getDigest(cartData) {
			var digest = {};
			cartData.forEach(function(item) {
				digest[item.courseId] = item.details.subject.subjectCode.toString();
			});
			return digest;
		}

		function compareDigests(digest1, digest2) {
			return angular.equals(digest1, digest2);
		}
	}

})();
