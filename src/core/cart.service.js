(function() {
	'use strict';

	angular
		.module('app.core')
		.factory('cartService', cartService);

	/* @ngInject */
	function cartService($http, $timeout, $q) {

		var service = {
			addToCart:				addToCart,
			getDegreePlans:			getDegreePlans,
			addToDegreePlan:		addToDegreePlan,
			getDegreePlanTerms:		getDegreePlanTerms,
			getFavorites:			getFavorites,
			removeFromFavorites:	removeFromFavorites,
			addToFavorites:			addToFavorites,
			removeFromCart:			removeFromCart,
			drop:					drop,
			getEnrolled:			getEnrolled,
			getCourse:				getCourse,
			getCart:				getCart,
			getCartTotal:			getCartTotal,
			enroll:					enroll,
			enrollSingle:			enrollSingle,
			getEnrollmentOptions:	getEnrollmentOptions,
			getEnrollmentStatus:	getEnrollmentStatus,
			swap:					swap
		};

		return service;

		function addToCart(termCode, subjectCode, courseId, classNumber, data) {
			var baseUrl = '/api/planner/v1/roadmap/' + termCode + '/' + subjectCode + '/' + courseId;

			if (classNumber) {
				baseUrl += '/' + classNumber;
			}
			return $http.post(baseUrl, data)
				.then(addCompleted)
				.catch(addFailed);

			function addCompleted(response) {
				return response.data;
			}

			function addFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function getDegreePlans() {
			return $http.get('/api/planner/v1/degreePlan', {})
				.then(getDegreePlansCompleted)
				.catch(getDegreePlansFailed);

			function getDegreePlansCompleted(response) {
				return response.data;
			}

			function getDegreePlansFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function addToDegreePlan(roadmapId, data) {
			return $http.post('/api/planner/v1/degreePlan/' + roadmapId + '/courses', data)
				.then(addToDegreePlanCompleted)
				.catch(addToDegreePlanFailed);

			function addToDegreePlanCompleted(response) {
				return response.data;
			}

			function addToDegreePlanFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function getDegreePlanTerms(roadmapId) {
			return $http.get('/api/planner/v1/degreePlan/' + roadmapId + '/termcourses/terms', {})
				.then(getDegreePlanTermsCompleted)
				.catch(getDegreePlanTermsFailed);

			function getDegreePlanTermsCompleted(response) {
				return response.data;
			}

			function getDegreePlanTermsFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function getFavorites() {
			return $http.get('/api/planner/v1/favorites', {})
				.then(getCourseCompleted)
				.catch(getCourseFailed);

			function getCourseCompleted(response) {
				return response.data;
			}

			function getCourseFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function addToFavorites(subjectCode, courseId) {
			return $http.post('/api/planner/v1/favorites/' + subjectCode + '/' + courseId, {})
				.then(addCompleted)
				.catch(addFailed);

			function addCompleted(response) {
				return response.data;
			}

			function addFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function removeFromFavorites(subjectCode, courseId) {
			return $http.delete('/api/planner/v1/favorites/' + subjectCode + '/' + courseId, {})
				.then(addCompleted)
				.catch(addFailed);

			function addCompleted(response) {
				return response.data;
			}

			function addFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function removeFromCart(termCode, subjectCode, courseId) {
			return $http.delete('/api/planner/v1/roadmap/' + termCode + '/' + subjectCode + '/' + courseId, {})
				.then(addCompleted)
				.catch(addFailed);

			function addCompleted(response) {
				return response.data;
			}

			function addFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function drop(termCode, data) {
			return $http.post('/api/enroll/v1/drop/' + termCode, data)
				.then(addCompleted)
				.catch(addFailed);

			function addCompleted(response) {
				return response.data;
			}

			function addFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function getCourse(termCode, subjectCode, courseId) {
			return $http.get('/api/search/v1/course/' + termCode + '/' + subjectCode + '/' + courseId, { cache: true })
				.then(getCourseCompleted)
				.catch(getCourseFailed);

			function getCourseCompleted(response) {
				return response.data;
			}

			function getCourseFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function getCart(term) {
			return $http.get('/api/planner/v1/roadmap/' + term)
				.then(getCartComplete)
				.catch(getCartFailed);

			function getCartComplete(response) {
				// Simulates slow cart request that is experienced at SOAR
				// var deferred = $q.defer();
				// $timeout(function() {
				// 	deferred.resolve(response.data);
				// }, 10000);
				// return deferred.promise;

				return response.data;
			}

			function getCartFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function getCartTotal(term) {
			return $http.get('/api/planner/v1/cartsummary/' + term)
				.then(getCartCountComplete)
				.catch(getCartCountFailed);

			function getCartCountComplete(response) {
				return response.data;
			}

			function getCartCountFailed(error) {
				return {
					'errorDescription': 'Error retrieving cart count',
					'error': error
				};
			}
		}

		function getEnrolled(termCode) {
			return $http.get('/api/enroll/v1/current/' + termCode, {})
				.then(getCartComplete)
				.catch(getCartFailed);

			function getCartComplete(response) {
				return response.data;
			}

			function getCartFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function enroll(termCode, data) {
			return $http.post('/api/enroll/v1/enroll/' + termCode, data)
				.then(getCartComplete)
				.catch(getCartFailed);

			function getCartComplete(response) {
				return response.data;
			}

			function getCartFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function enrollSingle(termCode, subjectCode, courseId, classNumber, data) {
			return $http.post('/api/enroll/v1/enroll/' + termCode + '/' + subjectCode + '/' + courseId + '/' + classNumber, data)
				.then(getCartComplete)
				.catch(getCartFailed);
			function getCartComplete(response) {
				return response.data;
			}
			function getCartFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		// ** No longer in use **
		function getEnrollmentOptions(termCode, subjectCode, courseId, sessionCode, classNumber) {
			// var enroll = {"honors":true,"waitlist":true,"classPermissionNumber":true};

			// var deferred = $q.defer();
			// deferred.resolve( enroll );
			// return deferred.promise;

			return $http.get('/api/enroll/v1/enrollmentOptions/' + termCode + '/' + subjectCode + '/' + courseId + '/' + sessionCode + '/' + classNumber, { cache: true })
				.then(getEnrollmentOptionsComplete)
				.catch(getEnrollmentOptionsFailed);

			function getEnrollmentOptionsComplete(response) {
				return response.data;
			}

			function getEnrollmentOptionsFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function getEnrollmentStatus() {
			return $http.get('/api/pending')
				.then(getEnrollmentStatusComplete)
				.catch(getEnrollmentStatusFailed);

			function getEnrollmentStatusComplete(response) {
				return response.data;
			}

			function getEnrollmentStatusFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function swap(termCode, addCourseId, addClassNumber, data) {
			return $http.post('/api/enroll/v1/swap/' + termCode + '/' + addCourseId + '/' + addClassNumber, data)
				.then(getEnrollmentOptionsComplete)
				.catch(getEnrollmentOptionsFailed);

			function getEnrollmentOptionsComplete(response) {
				return response.data;
			}

			function getEnrollmentOptionsFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

	}

})();
