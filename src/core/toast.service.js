(function() {
	'use strict';

	angular
		.module('app.core')
		.factory('toastService', toastService);

	/* @ngInject */
	function toastService($mdToast) {

		var service = {
			message: '',
			infoMessage: infoMessage,
			messageWithAction: messageWithAction
		};

		return service;

		function infoMessage(messageText, callback) {

			$mdToast.show({
				controller: function() {
					this.message = messageText;
				},
				controllerAs: 'vm',
				template:	'<md-toast>' +
								'<div id="toast-container">' +
									'<div class="md-toast-content">{{ vm.message }}</div>' +
								'</div>' +
							'</md-toast>',
				hideDelay: 3000,
				position: 'bottom center',
				action: callback
			});

		}

		// NOT CURRENTLY WORKING..WILL HAVE TO REVIEW MATERIAL SOURCE
		function messageWithAction(params) {
			$mdToast.show(params);
		}
	}

})();
