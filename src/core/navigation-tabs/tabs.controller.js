(function() {
	'use strict';

	angular
		.module('app.tabs')
		.controller('TabsController', TabsController);

	/* @ngInject */
	function TabsController($location, $mdDialog, $scope, constants) {
		var vm = this;
		vm.public = window.config && window.config.public || constants.public;

		vm.setActiveNav = setActiveNav;
		vm.betaDialog = betaDialog;

		function setActiveNav(path) {
			return ($location.$$path === path) ? 'active' : '';
		}

		function betaDialog(event) {
			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: event,
				templateUrl: 'components/dialogs/beta-dialog.html',
				clickOutsideToClose: false,
				multiple: true,
				scope: $scope,
				preserveScope: true,
				fullscreen: true,
				controller: BetaController
			});

			/* @ngInject */
			function BetaController($scope, $mdDialog) {

				vm.closeDialog = function() {
					$mdDialog.hide();
				};
			}
		}
	}

})();
