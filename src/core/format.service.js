(function() {
	'use strict';
	/**
	 * @desc Service used to format various pieces of data coming in from the back end
	 */
	angular
		.module('app.core')
		.factory('formatService', formatService)
		.filter('sectionFilter', function() {
			return function(elements, appliedFilters, specialGroups) {
				// We want the seats filters to be an 'OR' while the others are 'AND'
				function filterLogic(sectionAttribs, sectionFilters) {
					if ((sectionFilters.openSeats && sectionAttribs.hasOpenSeats) ||
						(sectionFilters.waitlistSeats && sectionAttribs.hasWaitlistSeats) ||
						(sectionFilters.closedSeats && sectionAttribs.hasClosedSeats)) {

						if(!sectionFilters.honors && !sectionFilters.reserved)
							return true;

						else if (sectionFilters.honors && !sectionFilters.reserved)
							return sectionAttribs.hasHonors;

						else if (!sectionFilters.honors && sectionFilters.reserved)
							return sectionAttribs.hasReserved;

						else return (sectionAttribs.hasHonors && sectionAttribs.hasReserved);
					}
				}
				// Only for courses with packages
				var filtered = (elements[0].hasOwnProperty('packages')) ?
					elements.filter(function(coursePackage) {
						var found = coursePackage.packages.filter(function(section){
							var sectionAttribs = {
								'hasOpenSeats' : section[0].packageEnrollmentStatus.status == 'OPEN',
								'hasWaitlistSeats' : section[0].packageEnrollmentStatus.status == 'WAITLISTED',
								'hasClosedSeats' : section[0].packageEnrollmentStatus.status == 'CLOSED',
								'hasHonors' : (section.length > 1) ? section[1].honors !== null : section[0].honors !== null,
								'hasReserved' : section[section.length-1].classAttributes.some(function(group) { return group.valueCode == specialGroups; }),
							}, sectionFilters = {
								'honors' : appliedFilters.indexOf('honors_honors') !== -1,
								'reserved' : appliedFilters.indexOf('reserved_reserved') !== -1,
								'openSeats' : appliedFilters.indexOf('seats_OPEN') !== -1,
								'waitlistSeats' : appliedFilters.indexOf('seats_WAITLISTED') !== -1,
								'closedSeats' : appliedFilters.indexOf('seats_CLOSED') !== -1,
							};

							return filterLogic(sectionAttribs, sectionFilters);
						});
						if (found.length) {
							coursePackage.packages = found;
							return true;
						}
					}) :
					// Only for courses with out packages
					elements.filter(function(section){
						var sectionAttribs = {
							'hasOpenSeats' : section.packageEnrollmentStatus.status == 'OPEN',
							'hasWaitlistSeats' : section.packageEnrollmentStatus.status == 'WAITLISTED',
							'hasClosedSeats' : section.packageEnrollmentStatus.status == 'CLOSED',
							'hasHonors' : section.honors !== null,
							'hasReserved' : section.classAttributes.some(function(group) { return group.valueCode == specialGroups; }),
						}, sectionFilters = {
							'honors' : appliedFilters.indexOf('honors_honors') !== -1,
							'reserved' : appliedFilters.indexOf('reserved_reserved') !== -1,
							'openSeats' : appliedFilters.indexOf('seats_OPEN') !== -1,
							'waitlistSeats' : appliedFilters.indexOf('seats_WAITLISTED') !== -1,
							'closedSeats' : appliedFilters.indexOf('seats_CLOSED') !== -1,
						};

						return filterLogic(sectionAttribs, sectionFilters);
					});

				return filtered;
			};
		});

	function formatService() {

		var service = {
			formatSubject: formatSubject,
			formatFootnotes: formatFootnotes,
			alphabetizeObjects: alphabetizeObjects,
		};

		return service;

		/**
		 * Format the formalDescription of a subject by capitalizing important words only
		 * @param subject String for the formalDescription
		 * @returns {string} The formatted formalDescription
		 */
		function formatSubject(subject) {
			if (subject) {
				var words = subject.split(/(\W)/);
				for (var i in words) {
					if (words.hasOwnProperty(i)) {
						if (words[i] != 'SC') {
							// Lowercase all
							words[i] = words[i].toLowerCase();
						}
						if (words[i] != 'and' && words[i] != 'of' && words[i] != 'in' && words[i] != 'the' && words[i] != 'to' && words[i] != 'that') {
							// Capitalize first letter
							words[i] = words[i].charAt(0).toUpperCase() + words[i].substr(1);
						}
					}
				}
				subject = words.join('');
				return subject;
			}

		}

		/**
		 * Format subject notes as HTML
		 * @param note A string for the note
		 * @returns {String} The note with HTML tags
		 */
		function formatFootnotes(note) {
			return note.replace(/\n/g, '<br>');
		}

		/**
		 * Sort array of objects alphabetically by the given property's value
		 * @param objects Array of objects to sort
		 * @param property String for the property to sort on
		 * @returns objects Array of sorted objects
		 */
		function alphabetizeObjects(objects, property) {
			objects.sort(function(a ,b) {
				if (a.hasOwnProperty(property) && b.hasOwnProperty(property)) {
					if (a[property] < b[property]) {
						return -1;
					} else if (a[property] > b[property]) {
						return 1;
					} else {
						return 0;
					}
				}
			});
			return objects;
		}

	}

})();