(function() {
	'use strict';

	angular
		.module('app.core')
		.factory('redirectInterceptor', redirectInterceptor);

	/* @ngInject */
	function redirectInterceptor($window, $q) {
		return {
			'responseError': function(rejection) {
				// When observing at 302, rejection.status is set to -1
				// typically the status is the http status code
				// so it seems this is an effective way to capture a 302
				// and reload the page since they are logged out.
				if (rejection.status === -1) {
					$window.location.reload();
				}
				return $q.reject(rejection);
			}
		};
	}

})();