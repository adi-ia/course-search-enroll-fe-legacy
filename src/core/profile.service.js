(function() {
	'use strict';
	/**
	 * @desc Service used to format various pieces of data coming in from the back end
	 */
	angular
		.module('app.core')
		.factory('profileService', profileService);

	/* @ngInject */
	function profileService($http) {

		var service = {
			getProfile: getProfile
		};

		return service;

		function getProfile(data) {
			return $http.get('/profile')
				.then(profileCompleted)
				.catch(profileFailed);

			function profileCompleted(response) {
				return response.data;
			}

			function profileFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

	}

})();