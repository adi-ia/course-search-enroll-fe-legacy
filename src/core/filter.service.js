(function() {
	'use strict';
	/**
	 * @desc Service used to construct elasticsearch filters from form data
	 */
	angular
		.module('app.core')
		.factory('filterService', filterService);


	function filterService() {

		var service = {
			filterList: {},
			addFilter: addFilter,
			removeFilter: removeFilter,
			clearFilters: clearFilters,
			buildFilterList: buildFilterList,
			addSimpleString: addSimpleString,
			addSimpleArray: addSimpleArray,
			addForeignLanguage: addForeignLanguage,
			addWorkforce: addWorkforce,
			addSpecialGroups: addSpecialGroups,
			addGenEd: addGenEd,
			addOpenSeats: addOpenSeats,
			addHonors: addHonors,
			addMinCredits: addMinCredits,
			addMaxCredits: addMaxCredits,
			addSession: addSession,
			addEthnic: addEthnic,
			addDayTime: addDayTime,
			addModeOfInstruction: addModeOfInstruction
		};

		return service;

		/**
		 * Add a filter to the list
		 * @param filter A JSON object for an elastic search filter
		 * @param filterName The name of the filter (collected in case a filter has to be removed)
		 */
		function addFilter(filter, filterName) {
			service.filterList[filterName] = filter;
		}

		/**
		 * [CURRENTLY UNUSED] Remove a filter from filter list (could be used in the future if live search
		 * is implemented, as filters will have to be added/removed in response to UI interactions)
		 * @param filterName The name of the filter to remove
		 */
		function removeFilter(filterName) {
			if (service.filterList[filterName]) {
				delete service.filterList[filterName];
			}
		}

		/**
		 * Reset the filter list
		 */
		function clearFilters() {
			service.filterList = {};
		}

		/**
		 * Build an array of filter objects based on filter list
		 * @returns {Array}
		 */
		function buildFilterList() {
			var filters = [];
			for (var key in service.filterList) {
				if (service.filterList.hasOwnProperty(key)) {
					filters.push(service.filterList[key]);
				}
			}
			return filters;
		}

		/**
		 * Used to add subject, online only, ethnic studies, and 50% grad work filters
		 * @param filterKey A string for the filter's key
		 * @param filterValue A string for the filter's value
		 * @param filterName A string for the filter's local identifier
		 */
		function addSimpleString(filterKey, filterValue, filterName) {
			var filter = { term: {} };
			filter.term[filterKey] = filterValue;
			service.addFilter(filter, filterName);
		}

		/**
		 * Used to add general education, breadth, course level, honors, and day filters
		 * @param filterKey A string for the filter's key
		 * @param filterValue An array for the filter's value
		 * @param filterName A string for the filter's local identifier
		 */
		function addSimpleArray(filterKey, filterValue, filterName) {
			var filter = { terms: {} };
			filter.terms[filterKey] = filterValue;
			service.addFilter(filter, filterName);
		}

		/**
		 * Build the 'open seats only' filter and add it to filter list
		 */
		function addForeignLanguage(valueCode) {
			var filter = {
				"query": {
					"match": {
						"foreignLanguage.code": valueCode
					}
				}
			};
			service.addFilter(filter, 'foreign-language-filter');
		}

		/**
		 * Build the 'open seats only' filter and add it to filter list
		 */
		function addWorkforce() {
			var filter = {
				"query": {
					"exists": {
						"field": "workplaceExperience"
					}
				}
			};
			service.addFilter(filter, 'workforce-filter');
		}

		/**
		 * Build the special groups filter and add it to filter list
		 * @param valueCode A string for classAttributes.valueCode
		 */
		function addSpecialGroups(valueCode) {
			var filter = {
				'has_child': {
					'type': 'class',
					'filter': {
						'term': {
							'classAttributes.valueCode': valueCode
						}
					}
				}
			};
			service.addFilter(filter, 'special-groups-filter');
		}

		/**
		 * Build the special groups com B filter and add it to filter list
		 */
		function addGenEd(valueCode, ethnicStudies, comB) {
			var filter = {
					"bool": {
						"should": [{
							"terms": {
								"generalEd.code": [
									valueCode
								]
							}
						}]
					}
				};

			if (ethnicStudies) {
				filter.bool.should.push({ "term": { "ethnicStudies.code": "ETHNIC ST" } });
			}

			if (comB) {
				filter.bool.should.push({ "has_child": { "type": "class", "query": { "term": { "comB": true } } } });
			}

			service.addFilter(filter, 'com-B-filter');
		}

		/**
		 * Build the 'open seats only' filter and add it to filter list
		 */
		function addOpenSeats(valueCode) {
			var filter = {
					"has_child": {
						"type": "enrollmentPackage",
						"query": {
							"match": {
								"packageEnrollmentStatus.status": valueCode
							}
						}
					}
			};
			service.addFilter(filter, 'open-seats-filter');
		}

		/**
		 * Build the 'open seats only' filter and add it to filter list
		 */
		function addHonors(valueCode) {
			var filter = {
			"or": [
				{
				"terms": {
					"honors": valueCode
				}
				},
				{
					"has_child": {
						"type": "class",
						"query": {
							"terms": {
								"honors": valueCode
							}
						}
					}
				}
			]
		};
			service.addFilter(filter, 'honors-filter');
		}

		function addModeOfInstruction(valueCode) {
			var filter = {
				"has_child": {
						"type": "enrollmentPackage",
						"query": {
							"multi_match" : {
									"query": valueCode,
									"type": "phrase",
									"fields": [ "sections.instructionMode" ]
							}
						}
					}
			};
			service.addFilter(filter, 'mode-of-instruction-filter');
		}

		/**
		 * Build min credits filter and add it to filter list
		 * @param credits A number for the credits
		 */
		function addMinCredits(credits) {
			var filter = {
				'range': {
					'minimumCredits': {
						'gte': credits
					}
				}
			};
			service.addFilter(filter, 'min-credits-filter');
		}

		/**
		 * Build max credits filter and add it to filter list
		 * @param credits A number for the credits
		 */
		function addMaxCredits(credits) {
			var filter = {
				'range': {
					'maximumCredits': {
						'lte': credits
					}
				}
			};
			service.addFilter(filter, 'max-credits-filter');
		}

		/**
		 * Build session filter and add it to filter list
		 * @param sessionCode A three-character string representing a session
		 */
		function addSession(sessionCode) {
			var filter = {
				'has_child': {
					'type': 'class',
					'query': {
						'term': {
							'sessionCode': sessionCode
						}
					}
				}
			};
			service.addFilter(filter, 'session-filter');
		}

		/**
		 * Build session filter and add it to filter list
		 * @param sessionCode A three-character string representing a session
		 */
		function addEthnic() {
			var filter = {
					'query': {
						'term': {
							'ethnicStudies.code': 'ETHNIC ST'
						}
					}
			};
			service.addFilter(filter, 'ethnic-studies');
		}

		/**
		 * Build day/time filters and add them to filter list
		 * @param days An array of day objects with 'name' and 'value' properties
		 * @param lowerBound
		 * @param upperBound
		 */
		function addDayTime(days, lowerBound, upperBound) {
			// TODO: FIND A BETTER WAY!
			var dayTimeFilters = [];

			// Build day filters
			for (var day in days) {
				var dayFilter = {};
				if (days.hasOwnProperty(day)) {
					if (days[day] != '') {
						dayFilter[ 'nestedClassMeetings.' + days[day] ] = true;
						dayTimeFilters.push( {'term': dayFilter} );
					}
				}
			}

			// Build time filters
			if (lowerBound) {
				var lowerRangeFilter = {
					'range': {
						'nestedClassMeetings.meetingTimeStart': {
							'gte': lowerBound
						}
					}
				};
				dayTimeFilters.push(lowerRangeFilter);
			}
			if (upperBound) {
				var upperRangeFilter = {
					'range': {
						'nestedClassMeetings.meetingTimeStart': {
							'lte': upperBound
						}
					}
				};
				dayTimeFilters.push(upperRangeFilter);
			}

			var filter = {
				'has_child' : {
					'type': 'enrollmentPackage',
					'filter': {
						'bool': {
							'must': [
								{
									'nested': {
										'path': 'nestedClassMeetings',
										'filter': {
											'bool': {
												'must': dayTimeFilters
											}
										}
									}
								}
							]
						}
					}
				}
			};
			service.addFilter(filter, 'day-time-filters');
		}
	}

})();