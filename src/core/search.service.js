(function() {
	'use strict';

	angular
		.module('app.core')
		.factory('searchService', searchService);

	/* @ngInject */
	function searchService($http, $q) {

		var service = {
			searchWithFilters: searchWithFilters,
			getTerms: getTerms,
			getSubjects: getSubjects,
			getSessionsByTerm: getSessionsByTerm,
			getSpecialGroupsByTerm: getSpecialGroupsByTerm,
			getEnrollmentPackage: getEnrollmentPackage,
			getEnrollmentPackageByNumber: getEnrollmentPackageByNumber,
			getSections: getSections,
			getCourseInfo: getCourseInfo,
			getCourse: getCourse,
			getPackages: getPackages,
			getSubjectsMap: getSubjectsMap,
			getEnrollmentOptions: getEnrollmentOptions
		};

		return service;

		/**
		 * Execute a search with the selected filters
		 * @param data A JSON object of the elastic search filters and search parameters
		 * @returns {*} Response object containing a list of course objects
		 */
		function searchWithFilters(data) {
			return $http.post('/api/search/v1', data)
				.then(searchCompleted)
				.catch(searchFailed);

			function searchCompleted(response) {
				return response.data;
			}

			function searchFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		/**
		 * Get currently searchable terms (semesters)
		 * @returns {*} Response object containing a list of terms
		 */
		function getTerms() {
			return $http.get('/api/search/v1/terms/', { cache:true })
				.then(getTermsComplete)
				.catch(getTermsFailed);

			function getTermsComplete(response) {
				return response.data;
			}

			function getTermsFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		/**
		 * Get currently searchable subjects
		 * @returns {*} Response object containing a list of subjects
		 */
		function getSubjects() {
			return $http.get('/api/search/v1/subjects/', { cache:true })
				.then(getSubjectsComplete)
				.catch(getSubjectsFailed);

			function getSubjectsComplete(response) {
				return response.data;
			}

			function getSubjectsFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		/**
		 * Get searchable sessions based on the term
		 * @param term A string for the term code
		 * @returns {*}
		 */
		function getSessionsByTerm(term) {
			return $http.get('/api/search/v1/sessions/' + term, { cache:true })
				.then(getSessionsByTermComplete)
				.catch(getSessionsByTermFailed);

			function getSessionsByTermComplete(response) {
				return response.data;
			}

			function getSessionsByTermFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		/**
		 * Get searchable special groups based on the term
		 * @param term A string for the term code
		 * @returns {*}
		 */
		function getSpecialGroupsByTerm(term) {
			return $http.get('/api/search/v1/specialgroups/' + term, { cache:true })
				.then(getSpecialGroupsByTermComplete)
				.catch(getSpecialGroupsByTermFailed);

			function getSpecialGroupsByTermComplete(response) {
				return response.data;
			}

			function getSpecialGroupsByTermFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		/**
		 * Get sections for a unique course
		 * @param term String for the term code
		 * @param courseId String for the course ID
		 * @param subjectCode String for the subject code
		 * @returns {*} JSON object of all matching sections
		 */
		function getSections(term, courseId, subjectCode) {
			return $http.get('/api/search/v1/sections/' + term + '/' + subjectCode + '/' + courseId, { cache:true })
				.then(getSectionComplete)
				.catch(getSectionFailed);

			function getSectionComplete(response) {
				return response.data;
			}

			function getSectionFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		/**
		 * Get course details from a class number
		 * @param term String for the term code
		 * @param classNumber String for the class number
		 * @returns {*} JSON object of all matching sections
		 */
		function getCourseInfo(term, classNumber) {
			return $http.get('/api/search/v1/class/' + term + '/' + classNumber, { })
				.then(getCourseInfoComplete)
				.catch(getCourseInfoFailed);

			function getCourseInfoComplete(response) {
				return response.data;
			}

			function getCourseInfoFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function getCourse(term, subjectCode, courseId) {
			return $http.get('/api/search/v1/course/' + term + '/' + subjectCode + '/' + courseId, { })
				.then(getCourseComplete)
				.catch(getCourseFailed);

			function getCourseComplete(response) {
				return response.data;
			}

			function getCourseFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		/**
		 * Get sections for a unique course
		 * @param term String for the term code
		 * @param courseId String for the course ID
		 * @param subjectCode String for the subject code
		 * @returns {*} JSON object of all matching sections
		 */
		function getEnrollmentPackage(term, sessionCode, courseId, classNumber) {
			return $http.get('/api/search/v1/enrollmentPackage/' + term + '/' + sessionCode + '/' + courseId + '/' + classNumber)
				.then(getSectionComplete)
				.catch(getSectionFailed);

			function getSectionComplete(response) {
				return response.data;
			}

			function getSectionFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function getEnrollmentPackageByNumber(term, query) {
			return $http.post('/api/search/v1/enrollmentPackage/' + term, query)
				.then(getSectionComplete)
				.catch(getSectionFailed);

			function getSectionComplete(response) {
				return response.data;
			}

			function getSectionFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		/**
		 * Get packages for a unique course
		 * @param term String for the term code
		 * @param courseId String for the course ID
		 * @param subjectCode String for the subject code
		 * @returns {*} JSON object of all matching packages
		 */
		function getPackages(term, courseId, subjectCode) {
			// var deferred = $q.defer();
			// deferred.resolve(JSON.parse(enroll));
			// return deferred.promise;

			return $http.get('/api/search/v1/enrollmentPackages/' + term + '/' + subjectCode + '/' + courseId)
				.then(getPackagesComplete)
				.catch(getPackagesFailed);

			function getPackagesComplete(response) {
				return response.data;
			}

			function getPackagesFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function getSubjectsMap() {
			return $http.get('api/search/v1/subjectsMap/0000', { cache:true })
				.then(function(response){ return response.data; })
				.catch(function(error) { return { 'errorDescription': 'Error retrieving data', 'error': error}; });
		}

		function getEnrollmentOptions(termCode, subjectCode, courseId, sessionCode, classNumber) {
			return $http.get('/api/enroll/v1/enrollmentOptions/' + termCode + '/' + subjectCode + '/' + courseId + '/' + sessionCode + '/' + classNumber, { })
				.then(getEnrollmentOptionsComplete)
				.catch(getEnrollmentOptionsFailed);

			function getEnrollmentOptionsComplete(response) {
				return response.data;
			}

			function getEnrollmentOptionsFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}
	}

})();