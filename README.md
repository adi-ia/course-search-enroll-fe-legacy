# Enrollment Front-End

This project is based on the best practices outlined [here by johnpapa](https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md). 
There are many new concepts and design patterns in this documentation so be sure to have a good understanding of its principles. 

## New Angular 2+ Front End
All new work should be written for the Angular 2+ Front End [here](https://git.doit.wisc.edu/adi-ia/course-search-enroll-fe). At this point we only have 2 of the 5 views complete in Angular 2+. Any changes to this repo have to be approved by the dev team as we transition fully to Angular 2+.


## Running

First time users, ensure you have node/npm installed, then run the following command:

    $ npm start

The front-end will run at [http://localhost:8001](http://localhost:8001), and will proxy API requests to the docker gateway.

In the future you can run:

	node express.js 
	
If you only want to start the front end server. 

## Deployment
To build the project's assets for production please run:

	gulp build:prod
	
This will build the site and place all files in the `/dist` folder. You do not need to do any building before a commit of new work since the site will be built by the CI/CD pipeline.

## Proxying
You do not need to run any of the backend infrastructure locally to develop the front end. All requests are being proxied to the test server (test.enroll.wisc.edu) 

## Mod Headers
Very much of the application will not work without being logged in as a student. To impersonate a student you will need a student login that has been approved for the test environment. Speak with the dev team and they will provide you with a proper student. Next, you will need to user a browser extension to include the appropriate headers. I suggest the ModHeaders extension for Chrome & Firefox. Add the following headers:

	uid: <test student value>
	wiscEduISISEmplID: <test student value>
	wiscEduPVI: <test student value>
	Authorization: <proxy auth string>
	X-API-Key: <web sockets auth string>
	

## Contributing

The front-end angular application can be found in `src/main/resources/static`.

The following configuration files are also relevant to the front end and are found in the project root:

 - `package.json`
 - `express.js`
 - `gulpfile.js`
 - `karma.conf.js`
 - `e2e-tests/protractor-conf.js`
 - `e2e-tests/scenarios.js`

## Gulp and local development
There are a number of gulp tasks to help you develop more easily please take a look at the `gulpfile.js`. After running an `npm start` you will need to open another terminal window to run gulp tasks. Running the `gulp` command will watch all js/sass files for changes and compile to:

`dist/css/app.css` 

`dist/js/app.js`
 
You can manually build the assets with `gulp build` and if you need to start the front end server: `node express.js` in this case you avoid `npm start` which installs all dependancies in the case you ready have them.


### Using debug 
Many variables in this project are being dumped to the javascript console. This is for convenience in debugging issues. There is a file within the project `src/core/debug.module.js` that sets the debug flag for the front end. By default its value is set to `DEBUG: false`, when you first run an `npm install` there a is gulp task to generate a new `debug.module.js` with `DEBUG: true`. This file is then ignored from version control and debugging will always be set to true.

### Public facing site
On the public test site `public-test.enroll.wisc.edu` we read in `config.js` from AWS to set a public flag that will only show the search view, and hide all cart actions. This does not work locally, however you can turn on the public view by looking in the `gulpfile.js` and search for public and setting that property to `true`. This will require a rebuild of the assets `gulp build`;

### Enable source maps
Since we are working with bundled javascript locally, you'll want to turn on sources maps for javascript and css. This will allow you to debug the bundled code by its correct filename and line number. As of Chrome 55, navigate to the dev tools "Settings" then from the "Preferences" -> "Sources" header check "Enable javascript source maps" and "Enable css source maps"

### Helpful hints
If you ever edit a file and do not see those changes locally, make sure the watch task is running. It's a good idea to keep an eye on your terminal in case of a parse error as well. The concat pipe will throw an error in your terminal and your browser will only serve a portion of the javascript up until the error, so the javascript error from the browser won't make sense. In most cases you'll be able to see the browser unable to load the page, but if you've changed a file that doesn't fire on load you could miss it. I encourage you to use a javascript linter to catch simple parse errors. I like the [sublime linter](http://www.sublimelinter.com/) but there are many others.  One other thing to note, if you add a javascript file to the project you do not have to include it in the `index.html` since the gulp javascript compile is bundling all relevant javascript. Only if you are adding third party modules, then you will need to include those.

### Version Updates
All javascript is versioned from the `config.json` file in the root of the project. This version reflects the version found in the `package.json`. Running `gulp bump` by default will bump a patch version (0.11.2 to 0.11.3). Supplying a `--semver` parameter with either `major` or `minor` will increment their respective values. For instance:

`gulp bump --semver major` bumps `0.11.2` to `1.11.2`

`gulp bump --semver minor` bumps `0.11.2` to `0.12.2`

This tasks creates a new `constants.module.js` that reads in the version number into the front end. This is so that `<script src="app.js?v=0.11.2"></script>` always updates to the correct version.
You will still have to edit the versions in the `package.json`  to the new value for a proper release. Then push your updates to the repo


## Testing

This app contains Unit tests (using [Karma](https://karma-runner.github.io/0.13/intro/how-it-works.html) 
with [Jasmine](http://jasmine.github.io/2.4/introduction.html)) and End to End tests (using [Protractor](http://www.protractortest.org/#/).

### Running Unit Tests

These tests are written in Jasmine and executed by the Karma test runner.

- Configuration is found in `karma.conf.js`
- Jasmine unit tests: Each `view-` directory has a `___.test.js` file along with its controller, module, and HTML files.

Run unit tests with the npm script:

    npm test


### Running End to End Tests

These tests are written in Jasmine and executed by the Protractor end-to-end test runner.

- Configuration is found in `e2e-tests/protractor-conf.js`
- End-to-end tests are found in `e2e-tests/scenarios.js`

Run end-to-end tests with the npm script:

    npm run protractor
